package com.example.mymap;

import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.RelativeLayout;
import android.widget.Switch;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import static java.lang.Integer.parseInt;


public class SavedPlaceFragment extends Fragment {
    Database database;
    List<SavedPlace> arrayData;
    SavedPlaceAdapter adapter;
    private RecyclerView recyclerView;
    static int isRenew=0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RelativeLayout layout_1 = (RelativeLayout)inflater.inflate(R.layout.layout_fragment_place_saved,null);
        View noPlace = layout_1.findViewById(R.id.noPlace);
        //Load lại data từ database local
        getData();
        /*Nếu người dùng mở lần đầu - hoặc người dùng tắt ứng dụng và mở lại thì
         khôi phục lại các biến từ database local*/
//        if (isRenew==0)
//            restoreVariable();
        if(arrayData.size()==0)noPlace.setVisibility(View.VISIBLE);
        recyclerView = layout_1.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter= new SavedPlaceAdapter(getActivity(),R.layout.customlist1,arrayData);
        recyclerView.setAdapter(adapter);
        isRenew++;
        return layout_1;
    }

//    private void restoreVariable() {
//        database=new Database(getActivity(),"database.sqlite",null,1);
//        Cursor dataSavedPlace=database.GetData("select* from SavedPlace");
//        FormSavePlaceActivity.alarmManager=new ArrayList<>();
//        while(dataSavedPlace.moveToNext()){
//            String id=dataSavedPlace.getString(0);
//            String name=dataSavedPlace.getString(1);
//            String note=dataSavedPlace.getString(2);
//            String address=dataSavedPlace.getString(3);
//            String latitude=dataSavedPlace.getString(4);
//            String longitude=dataSavedPlace.getString(5);
//            String AlarmTime=dataSavedPlace.getString(6);
//            int IsChecked=parseInt(dataSavedPlace.getString(7));
//            FormSavePlaceActivity.alarmManager
//        }
//    }

    private  void getData(){
        database=new Database(getActivity(),"database.sqlite",null,1);
        arrayData=new ArrayList<>();
        Cursor dataSavedPlace=database.GetData("select* from SavedPlace");
        while(dataSavedPlace.moveToNext()){
            String id=dataSavedPlace.getString(0);
            String name=dataSavedPlace.getString(1);
            String note=dataSavedPlace.getString(2);
            String address=dataSavedPlace.getString(3);
            String latitude=dataSavedPlace.getString(4);
            String longitude=dataSavedPlace.getString(5);
            String AlarmTime=dataSavedPlace.getString(6);
            int IsChecked=parseInt(dataSavedPlace.getString(7));
            arrayData.add(new SavedPlace(id,name,note,address,latitude,longitude,AlarmTime,IsChecked));
        }
    }
}
