package com.example.mymap;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {
    MapsActivity mapsActivity=new MapsActivity();
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(MapsActivity.MY_ACTION)){
            String string_extra=intent.getStringExtra("extra");
            String isCancle=intent.getStringExtra("extra1");
            String name=intent.getStringExtra("name");
            String note=intent.getStringExtra("note");
            String time=intent.getStringExtra("time");
            String address = intent.getStringExtra("address");
            int isChecked=intent.getIntExtra("ischecked",-1);
            int id=intent.getIntExtra("id",-1);
            int index=intent.getIntExtra("index",-1);
            int isDeleteMedia=intent.getIntExtra("deletemedia",0);
            Log.e("Am thanh","Tao am thanh " + index);
            Intent myIntent1=new Intent(context,Sound.class);
            myIntent1.putExtra("index",index);
            myIntent1.putExtra("extra",string_extra);
            myIntent1.putExtra("deletemedia",isDeleteMedia);
            context.startService(myIntent1);
            //Thông báo notification
            if(string_extra.equals("on")){
                Log.e("Notification","Tao notification " + index);
                Intent myIntent=new Intent(context,AlarmService.class);
                context.startService(myIntent);
            }
            //Nếu string_extra = on thì
            // Hiển thị màn hình MapsActivity và chuyển sang savedplaced fragment
            if(isCancle==null){
                Intent i=new Intent(context,mapsActivity.getClass());
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("open","openfragment");
                i.putExtra("name",name);
                i.putExtra("note",note);
                i.putExtra("time",time);
                i.putExtra("ischecked",isChecked);
                i.putExtra("id",id);
                i.putExtra("address",address);
                if(index!=-1) i.putExtra("index",index);
                context.startActivity(i);
            }
        }
    }
}


