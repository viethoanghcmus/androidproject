package com.example.mymap.UserInfo;

import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.mymap.SavedPlace;
import com.example.mymap.UserInfo.UserInfo;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseError;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class UserInfoDAO {

    public  static String UserInfoRef = "UserInfo";
    public   static String EmailRef = "Email";
    public   static String mGroupRef = "mGroup";
    public  static String DisplayNameRef = "DisplayName";

    public static FirebaseUser fireUser = FirebaseAuth.getInstance().getCurrentUser();
    private static FirebaseDatabase database = FirebaseDatabase.getInstance();

    public static DatabaseReference UserInfoServer = database.getReference().child(UserInfoRef);
    public static String mGroup=null;




    //Lấy thông tin user login từ server
    public static UserInfo GetCurrentUserInfo() {

        UserInfo user = new UserInfo();
        if (fireUser != null) {
            user.UID = fireUser.getUid();
        }
        return user;
    }



    public static boolean UpdateNameDisplay(String UID, String newDisplayName) {

        UserInfoServer.child(UID).child(DisplayNameRef).setValue(newDisplayName);
        return true;
    }

    //Cập nhập trạng thái nhóm của User
    //Group=0, nếu không trong nhóm nào
    public static boolean UpdateGroup(String UID,String GroupID) {

        UserInfoServer.child(UID).child(mGroupRef).setValue(GroupID);
        return true;
    }

    public static boolean UpdateEmail(String UID,String newEmailName) {
        UserInfoServer.child(UID).child(EmailRef).setValue(newEmailName);
        return true;
    }

    public interface OnGetDataListener {
        //this is for callbacks
        void onSuccess(DataSnapshot dataSnapshot);
        void onStart();
        void onFailure();
    }

    //    Tương tự bên UserDataDAO, lấy về danh sách savePlace
    public static String GetMGroup(final String UID) {
        Log.d("zzz123", "GetMGroup: 0");
        FirebaseDatabase.getInstance().getReference("UserInfo").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mGroup = dataSnapshot.child(UID).child("mGroup").getValue(String.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "onCancelled", databaseError.toException());
                // ...
            }
        });
        Log.d("zzz123", "GetMGroup: 0"+mGroup);
        return mGroup;
    }
}
