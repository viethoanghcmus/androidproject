package com.example.mymap;

public class Places {
    private int Icon;
    private String Name;

    Places(int icon, String name) {
        Icon = icon;
        Name = name;
    }

    public void setIcon(int icon) {
        Icon = icon;
    }
    public void setName(String name) {
        Name = name;
    }
    int getIcon() {
        return Icon;
    }
    public String getName() {
        return Name;
    }
}
