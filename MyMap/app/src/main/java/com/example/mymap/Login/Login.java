package com.example.mymap.Login;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mymap.Database;
import com.example.mymap.FormSavePlaceActivity;
import com.example.mymap.GroupData.GroupDAO;
import com.example.mymap.MapsActivity;
import com.example.mymap.R;
import com.example.mymap.Sound;
import com.example.mymap.UserData.SavedPlaceDAO;
import com.example.mymap.UserData.UserDataDAO;
import com.example.mymap.UserInfo.UserInfoDAO;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;


public class Login extends AppCompatActivity implements View.OnClickListener {
    Button registerButton;
    Button LoginButton;
    Button ResetPassButton;
    EditText emailButton;
    EditText inputPassword;
    private FirebaseAuth mAuth;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();
        progressBar = findViewById(R.id.progressBar);
        registerButton = findViewById(R.id.registerButton);
        LoginButton = findViewById(R.id.LoginButton);
        ResetPassButton = findViewById(R.id.btn_reset_password);
        emailButton = findViewById(R.id.EmailEditText);
        inputPassword = findViewById(R.id.passEditText);
        registerButton.setOnClickListener(this);
        LoginButton.setOnClickListener(this);
        ResetPassButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.registerButton: {
                Intent intent = new Intent(Login.this, Register.class);
                startActivity(intent);
                break;
            }
            case R.id.LoginButton: {
                Login();
                break;
            }
            case R.id.btn_reset_password: {
                Intent intent = new Intent(Login.this, ResetPassword.class);
                startActivity(intent);
                break;
            }
        }
    }


    private void Login() {
        progressBar.setVisibility(View.VISIBLE);
        String email = emailButton.getText().toString();
        final String password = inputPassword.getText().toString();
        if (email.length() == 0 || password.length() == 0) {
            Toast.makeText(getApplicationContext(), "Email và mật khẩu không được để trống !",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(Login.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        progressBar.setVisibility(View.GONE);
                        if (!task.isSuccessful()) {
                            // there was an error
                            if (password.length() < 6) {
                                inputPassword.setError(getString(R.string.minimum_password));
                            } else {
                                Toast.makeText(Login.this, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                            }
                        } else {
                            //Login success
                            DeleteAllAlarm();
                            //Đăng nhập thành công
                            Database db = new Database(getApplication(),"database.sqlite",null,1);
                            SavedPlaceDAO savedPlaceDAO = new SavedPlaceDAO(db);
                            savedPlaceDAO.GetSavedPlaceDataAndSetAlarm(FirebaseAuth.getInstance().getCurrentUser().getUid(),getApplicationContext());
                            GroupDAO.GetMyGroup(FirebaseAuth.getInstance().getCurrentUser().getUid());
                            //Toast.makeText(Login.this, GroupDAO.GetMyGroup(FirebaseAuth.getInstance().getCurrentUser().getUid()), Toast.LENGTH_LONG).show();
                            finish();
                        }
                    }
                });
    }

    public static void DeleteAllAlarm(){
        for(int i=0;i < MapsActivity.number;i++){
            FormSavePlaceActivity.alarmManager.get(0).cancel(FormSavePlaceActivity.arrayPendingIntent.get(0));
            FormSavePlaceActivity.alarmManager.remove(0);
            FormSavePlaceActivity.arrayPendingIntent.remove(0);
            FormSavePlaceActivity.switch1.remove(0);
            FormSavePlaceActivity.calendars.remove(0);
            FormSavePlaceActivity.Name.remove(0);
            FormSavePlaceActivity.Note.remove(0);
            FormSavePlaceActivity.Time.remove(0);
            FormSavePlaceActivity.Address.remove(0);
            FormSavePlaceActivity.IsChecked.remove(0);
            FormSavePlaceActivity.Id.remove(0);
            FormSavePlaceActivity.TimeInstance.remove(0);
            Sound.mediaPlayers.remove(0);
        }
        //Renew
        FormSavePlaceActivity.alarmManager=new ArrayList<>();
        FormSavePlaceActivity.alarmManager=new ArrayList<>();
        FormSavePlaceActivity.arrayPendingIntent=new ArrayList<>();
        FormSavePlaceActivity.switch1=new ArrayList<>();
        FormSavePlaceActivity.calendars=new ArrayList<>();
        FormSavePlaceActivity.Name=new ArrayList<>();
        FormSavePlaceActivity.Note=new ArrayList<>();
        FormSavePlaceActivity.Time=new ArrayList<>();
        FormSavePlaceActivity.Address=new ArrayList<>();
        FormSavePlaceActivity.IsChecked=new ArrayList<>();
        FormSavePlaceActivity.Id=new ArrayList<>();
        FormSavePlaceActivity.TimeInstance=new ArrayList<>();
        Sound.mediaPlayers=new ArrayList<>();
        MapsActivity.number=0;
    }


}
