package com.example.mymap.UserData;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.util.Log;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mymap.AlarmReceiver;
import com.example.mymap.Database;
import com.example.mymap.FormSavePlaceActivity;
import com.example.mymap.MapsActivity;
import com.example.mymap.R;
import com.example.mymap.SavedPlace;
import com.example.mymap.Sound;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import static android.content.Context.ALARM_SERVICE;
import static androidx.constraintlayout.widget.Constraints.TAG;
import static java.lang.Integer.parseInt;


public  class SavedPlaceDAO {
    private static Database database;
    private Context context;

    public  void setDatabase(Database database) {
        SavedPlaceDAO.database = database;
    }

    public static Database getDatabase() {
        return database;
    }

    public  SavedPlaceDAO(Database database) {
        SavedPlaceDAO.database = database;
    }

    public static String Ref = "SavedPlace";
    //cập nhập dữ liệu lên server
    public static void UploadData(SavedPlace savedPlace) {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference myRef = db.getReference(Ref);

        myRef.child(FirebaseAuth.getInstance().
                getCurrentUser().getUid()).child(String.valueOf(savedPlace.getId())).setValue(savedPlace);
    }

    public  void PushSavedPlaceData() {
        FirebaseDatabase tmpDb = FirebaseDatabase.getInstance();
        DatabaseReference myRef = tmpDb.getReference("SavedPlace");
        if ( FirebaseAuth.getInstance().getCurrentUser()!=null) {
            final String tmpUID=FirebaseAuth.getInstance().getCurrentUser().getUid();
            myRef.child(tmpUID).removeValue();
            Cursor savedPlace = database.GetData("select* from SavedPlace");
            while (savedPlace.moveToNext()) {
                String id = savedPlace.getString(0);
                String name = savedPlace.getString(1);
                String note = savedPlace.getString(2);
                String address = savedPlace.getString(3);
                String latitude = savedPlace.getString(4);
                String longitude = savedPlace.getString(5);
                String alarmTime = savedPlace.getString(6);
                int isChecked = parseInt(savedPlace.getString(7));
                SavedPlace data = new SavedPlace(id, name, note, address, latitude, longitude, alarmTime, isChecked);
                UploadData(data);
            }
        }

    }
    //    Tương tự bên UserDataDAO, lấy về danh sách savePlace
    public static void GetSavedPlaceDataAndSetAlarm(final String UID, final Context context)
    {
        FirebaseDatabase.getInstance().getReference(Ref).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //DROP DATABASE IF EXISTS
                if(database!=null) database.ClearTableSavedPlace();
                // Get Post object and use the values to update the UI
                List<SavedPlace> listPlace = new ArrayList<>();
                for (DataSnapshot child : dataSnapshot.child(UID).getChildren()) {
                    String address = child.child("address").getValue(String.class);
                    String name = child.child("name").getValue(String.class);
                    String note = child.child("note").getValue(String.class);
                    String alarmTime = child.child("alarmTime").getValue(String.class);
                    String id = child.child("id").getValue(String.class);
                    String longitude = child.child("longitude").getValue(String.class);
                    String latitude = child.child("latitude").getValue(String.class);
                    Integer isChecked = child.child("isChecked").getValue(Integer.class);
                    listPlace.add(new SavedPlace(id, name, note, address, latitude, longitude, alarmTime, isChecked));

                }
//                 Xử lý listPlace;
                for (int j = 0; j < listPlace.size(); j++) {
                    database.InsertSavedPlace(listPlace.get(j));
                }

                for(int i=0;i<listPlace.size();i++){
                    FormSavePlaceActivity.Id.add(listPlace.get(i).getId());
                    FormSavePlaceActivity.Name.add(listPlace.get(i).getName());
                    FormSavePlaceActivity.Note.add(listPlace.get(i).getNote());
                    FormSavePlaceActivity.Address.add(listPlace.get(i).getAddress());
                    FormSavePlaceActivity.Time.add(listPlace.get(i).getAlarmTime());
                    FormSavePlaceActivity.IsChecked.add(String.valueOf(listPlace.get(i).getIsChecked()));
                    Calendar calendar1=Calendar.getInstance();
                    int mDay=parseInt(listPlace.get(i).getAlarmTime().substring(0,2));
                    int mMonth=parseInt(listPlace.get(i).getAlarmTime().substring(3,5));
                    int mYear=parseInt(listPlace.get(i).getAlarmTime().substring(6,10));
                    int mHour=parseInt(listPlace.get(i).getAlarmTime().substring(11,13));
                    int mMinute=parseInt(listPlace.get(i).getAlarmTime().substring(14,16));
                    calendar1.set(Calendar.DAY_OF_MONTH,mDay);
                    calendar1.set(Calendar.MONTH,mMonth-1);
                    calendar1.set(Calendar.YEAR,mYear);
                    calendar1.set(Calendar.HOUR_OF_DAY,mHour);
                    calendar1.set(Calendar.MINUTE,mMinute);
                    FormSavePlaceActivity.calendars.add(calendar1);
                    Calendar calendar=Calendar.getInstance();
                    FormSavePlaceActivity.TimeInstance.add(calendar);

                    //Set alarm
                    Intent intent = new Intent(context, AlarmReceiver.class);
                    intent.putExtra("name",listPlace.get(i).getName());
                    intent.putExtra("note",listPlace.get(i).getNote());
                    intent.putExtra("address",listPlace.get(i).getAddress());
                    intent.putExtra("time",listPlace.get(i).getAlarmTime());
                    intent.putExtra("ischecked",listPlace.get(i).getIsChecked());
                    intent.putExtra("id",i + 1);
                    intent.putExtra("extra","on");
                    intent.putExtra("index",i);
                    Switch swt=new Switch(context);
                    swt.setChecked(listPlace.get(i).getIsChecked() == 1);
                    FormSavePlaceActivity.switch1.add(swt);
                    FormSavePlaceActivity.switch1.get(i).setChecked(listPlace.get(i).getIsChecked() == 1);
                    PendingIntent pendingIntent= PendingIntent.getBroadcast(context,
                            i,intent,PendingIntent.FLAG_UPDATE_CURRENT);
                    FormSavePlaceActivity.arrayPendingIntent.add(pendingIntent);
                    FormSavePlaceActivity.alarmManager.add((AlarmManager)context.getSystemService(ALARM_SERVICE));
                    Sound.mediaPlayers.add(MediaPlayer.create(context, R.raw.nhac));
                    if (FormSavePlaceActivity.switch1.get(i).isChecked()){
                        if (FormSavePlaceActivity.calendars.get(i).getTimeInMillis()
                                >= FormSavePlaceActivity.TimeInstance.get(i).getTimeInMillis()){
                            FormSavePlaceActivity.alarmManager.get(i).set(AlarmManager.RTC_WAKEUP,
                                    FormSavePlaceActivity.calendars.get(i).getTimeInMillis()
                                    ,FormSavePlaceActivity.arrayPendingIntent.get(i));
                            Log.e("On","Đã bật âm báo " + i);
                        }
                    }
                    MapsActivity.number++;
                }

                //Mở sang màn hình home
                MapsActivity mapsActivity=new MapsActivity();
                Intent i=new Intent(context,mapsActivity.getClass());
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "onCancelled", databaseError.toException());
                // ...
            }
        });
//        DatabaseReference myRef = db.getReference(Ref);
//        myRef.addValueEventListener(userDataListener);
    }

}
