package com.example.mymap.UserData;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.mymap.SavedPlace;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class MyGroupDAO {
    public static String myGroupRef = "mGroup";
    public static void UpdateMyGroup(String UID,MyGroup mGroupData) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference GroupRef = database.getReference("User").child(UID);
        GroupRef.child(myGroupRef).setValue(mGroupData);
    }

    //Custom lại hàm này để lấy dữ liệu về
    public static void GetMyGroup(final String UID) {

        FirebaseDatabase database = FirebaseDatabase.getInstance();

        ValueEventListener userDataListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                // Get Post object and use the values to update the UI
                MyGroup myGroup = dataSnapshot.child(UID).getValue(MyGroup.class);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message

                // ...
            }
        };
        DatabaseReference myRef = database.getReference("User").child(myGroupRef);
        myRef.addListenerForSingleValueEvent(userDataListener);
    }


}
