package com.example.mymap;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.example.mymap.GroupData.Group;
import com.google.firebase.database.ValueEventListener;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.mymap.GroupData.GroupDAO;
import com.example.mymap.GroupData.MemberGroup;
import com.example.mymap.Login.Login;
import com.example.mymap.UserData.SavedPlaceDAO;
import com.example.mymap.UserData.UserData;
import com.example.mymap.UserData.UserDataDAO;
import com.example.mymap.UserInfo.UserInfo;
import com.example.mymap.UserInfo.UserInfoDAO;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.security.auth.Destroyable;

import de.hdodenhof.circleimageview.CircleImageView;

import static java.io.File.createTempFile;
import static java.lang.Integer.parseInt;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, NavigationView.OnNavigationItemSelectedListener {
    private static final int Request_code = 101;
    private static final int Request_code2 = 102;
    public static final String MY_ACTION="istime.com";
    //VAR MAP
    public static GoogleMap mMap;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    FusedLocationProviderClient fusedLocationProviderClient;
    Database database;
    Location mLocation;
    Object dataTransfer[];
    GetNearbyPlacesData getNearbyPlacesData;
    String url;
    public static int number=0;
    static int a=0;
    Marker mkLocation;
    Marker mkMem1;
    Group myGroup = new Group();
    //VAR UI
    private DrawerLayout drawer;
    BottomNavigationView navigation;
    NavigationView navigationView;
    AlertDialog.Builder builder;
    AlertDialog.Builder builder2;
    ImageButton editName;
    Uri avatarUri;
    Uri mem1Uri = Uri.parse("android.resource://com.example.mymap/drawable/hieuho");
    View nav_header;
    CircleImageView avatar;
    UserInfo userInfo;
    TextView nameDisplay;
    TextView Email;
    Button btnLogin;
    ImageButton btnEidtName;


    int action = 0;
    boolean initMap = false;
    boolean shareMem1 = true;
    public Context mContext = MapsActivity.this;
    List<MemberGroup> memberGroupList;
    public List<Marker> markerList;
    int indexMember;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //LƯU ĐỊA ĐIỂM VÀO DATABASE LOCAL
        database = new Database(this, "database.sqlite", null, 1);
        //Tạo bảng SavedPlace nếu chưa tồn tại
        database.CreateTableSavedPlace();
        database.CreateMemberGroup();
        //Restore lại các biến đã mất
        if (a == 0) {
            restoreVariable();
            a++;
        }

        setContentView(R.layout.activity_maps);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        Button btnFind = findViewById(R.id.btnFind);
        btnFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FindPlaceInList.class);
                startActivityForResult(intent, FindPlaceInList.RETURN_CODE);
            }
        });

        //NAVIGATION BAR BOTTOM - MENU HIỂN THỊ BÊN DƯỚI
        navigation = findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.navigation_home);//Để mặc định là: Trang chủ
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //ACTION BAR - THANH TIÊU ĐỀ Ở TRÊN
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setSubtitle("@produce by Team7");
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        nav_header = navigationView.getHeaderView(0);
        btnLogin = nav_header.findViewById(R.id.LoginButton);
        Email = nav_header.findViewById(R.id.email);
        avatar = nav_header.findViewById(R.id.avatar);
        nameDisplay = nav_header.findViewById(R.id.name);
        btnEidtName = nav_header.findViewById(R.id.edit_name);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setClass(MapsActivity.this, Login.class);
                startActivity(i);
            }
        });
        Email.setVisibility(View.GONE);
        nameDisplay.setVisibility(View.GONE);

        locationRequest = new LocationRequest();
        locationRequest.setInterval(7500); //use a value fo about 10 to 15s for a real app
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Task<Location> task = fusedLocationProviderClient.getLastLocation();
            task.addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    mLocation = location;
                    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.map);
                    mapFragment.getMapAsync(MapsActivity.this);
                }
            });
        } else {
            // request permissions
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Request_code);
            }
        }

        //Callback- Nhận loaction khi có sự thay đổi
        markerList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            markerList = new ArrayList<>();
            markerList.add(null);
            markerList.add(null);
            markerList.add(null);
            markerList.add(null);
            markerList.add(null);
        }
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                for (Location location : locationResult.getLocations()) {

                    if (location != null) {
                        mLocation = location;
                        //update marker
                        if (initMap) {
                            if (mkLocation != null) {
                                mkLocation.remove();
                                LatLng latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
                                MarkerOptions markerOptions = new MarkerOptions().position(latLng).title("Vị trí của tôi");
                                //chèn avatar location
                                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(createCustomMarker(MapsActivity.this,
                                        avatarUri, "VH")));
                                mkLocation = mMap.addMarker(markerOptions);
                                //update location member trong group
                            }
                            //day location len server lien tuc
                            UserData mUserData = new UserData(mLocation.getLatitude(), mLocation.getLongitude());
                            UserDataDAO.UploadUserData(mUserData);


                        }
                    }
                }
            }
        };
        //Lấy dữ liệu từ Alarm Receiver gửi về
        //Mở fragment savedplace khi có tiếng nhạc báo
        //Alert Dialog thông báo có muốn tắt nhạc báo hay không
        String open = getIntent().getStringExtra("open");
        final int index = getIntent().getIntExtra("index", -1);
        final String name = getIntent().getStringExtra("name");
        String name1 = "<b>" + name + "</b>";
        final String note = getIntent().getStringExtra("note");
        final String time = getIntent().getStringExtra("time");
        String address = getIntent().getStringExtra("address");
        final int isChecked = getIntent().getIntExtra("ischecked", -1);
        final int id1 = getIntent().getIntExtra("id", -1);
        if (open != null) {
            if (open.equals("openfragment")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Thông báo")
                        .setCancelable(true)
                        .setIcon(R.drawable.ic_location)
                        .setMessage("Bạn có lời nhắc đến địa điểm: " + Html.fromHtml(name1) + "\n" +
                                "Địa chỉ: " + address + "\n" +
                                "Vào lúc: " + time)
                        .setPositiveButton("Tắt", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                FormSavePlaceActivity.alarmManager.get(index).cancel(FormSavePlaceActivity.arrayPendingIntent.get(index));
                                FormSavePlaceActivity.switch1.get(index).setChecked(false);
                                Database db = new Database(MapsActivity.this, "database.sqlite", null, 1);
                                db.QueryData("UPDATE SavedPlace set name='" + name
                                        + "',note='" + note + "',AlarmTime='" + time + "'" +
                                        ",IsChecked='" + 0 + "' WHERE id='" + id1 + "'");
                                navigation.setSelectedItemId(R.id.navigation_place_saved);
                                Intent intent = new Intent(MapsActivity.this, AlarmReceiver.class);
                                intent.putExtra("extra", "off");
                                intent.putExtra("extra1", "cancel");
                                intent.putExtra("index", index);
                                sendBroadcast(intent);
                            }
                        })
                        .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                navigation.setSelectedItemId(R.id.navigation_place_saved);
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }


    public void loadMemberGroupLocation(){
        String nameMember;
        Uri avatarMember;
        //get group id ve, get trong userinfo
        if (FirebaseAuth.getInstance().getCurrentUser()!= null)
        {
            ValueEventListener myGroupListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    indexMember =0;
                    // Get Post object and use the values to update the UI
                    myGroup.GID = (String) dataSnapshot.child(UserInfoDAO.mGroupRef).getValue();


                    //lấy list members
                    ValueEventListener memberListener = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {


                            // Code xử lý ở đây

                            //memberDataList = new ArrayList<>();
                            //Lấy tên nhóm
                            //Lấy UID va ShareStatus của listMemberGroup
                            indexMember=0;
                            for (DataSnapshot memberSnapshot : dataSnapshot.child(GroupDAO.StrMembersRef).getChildren()) {
                                MemberGroup member = new MemberGroup();
                                member.setUID(memberSnapshot.getKey());

                                if (memberSnapshot.child(GroupDAO.StrMembersShare).getValue() != null) {
                                    member.setShareLocation((boolean) memberSnapshot.child(GroupDAO.StrMembersShare).getValue());

                                }

                                myGroup.listMembers.add(member);
                                Log.d("logadd", "onDataChange: "+ myGroup.listMembers.size());

                            }
                            //Get Name, Email
                            for (final MemberGroup member : myGroup.listMembers) {

                                ValueEventListener userInfoListener = new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        member.setName(dataSnapshot.child(UserInfoDAO.DisplayNameRef).getValue().toString());
                                        member.setEmail(dataSnapshot.child(UserInfoDAO.EmailRef).getValue().toString());

                                        Log.d("Group","name: "+ member.getName());

                                        if (member.isShareLocation()){

                                            ValueEventListener userDataListener = new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {

                                                    // Get Post object and use the values to update the UI
                                                    double _lat = (double)dataSnapshot.child("_lat").getValue();
                                                    double _long = (double)dataSnapshot.child("_long").getValue();
                                                    //if (userData.get_lat()!=null & userData.get_long()!=null)
                                                    {

                                                    }
                                                    Log.d("logadd", "onDataChange markerlist: "+markerList.size());

                                                    //Marker memberMarker;
                                                    if (member.getMarker() != null) {
                                                        member.getMarker().remove();
                                                    }

                                                    Log.d("Group","lat: "+ _lat + "Long: " + _long);
                                                    LatLng memberLocation = new LatLng(_lat,_long);

                                                    member.setMarker(MapsActivity.mMap.addMarker(new MarkerOptions().position(memberLocation).title(member.getName())
                                                            .icon(BitmapDescriptorFactory.fromBitmap(createCustomMarker(MapsActivity.this,
                                                                    member.getAvatar(), member.getName())))));
//                                                    markerList.set(indexMember,MapsActivity.mMap.addMarker(new MarkerOptions().position(memberLocation).title(member.getName())
//                                                            .icon(BitmapDescriptorFactory.fromBitmap(createCustomMarker(MapsActivity.this,
//                                                                    member.getAvatar(), member.getName())))));
                                                    //if (indexMember<myGroup.listMembers.size()) indexMember++;
                                                }
                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {
                                                    // Getting Post failed, log a message

                                                    // ...
                                                }
                                            };
                                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                                            DatabaseReference myRef = database.getReference().child(UserDataDAO.UserDataRef).child(member.getUID());
                                            myRef.addValueEventListener(userDataListener);



                                        }

                                        // hình load k đc
                                        FirebaseStorage storage = FirebaseStorage.getInstance();
                                        String fileName = "avatar.jpg";


                                        StorageReference storageRef = storage.getReference().child(member.getUID()).child(fileName);

                                        final File localFile;
                                        try {


                                            localFile = File.createTempFile(member.getUID(), "jpg");
                                            storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                                @Override
                                                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                                    // Local temp file has been created
                                                    Uri avatar_Uri = Uri.fromFile(localFile);
                                                    member.setAvatar(avatar_Uri);

                                                    //Đã lấy đc full dữ liệu
                                                    //Xử lí hiện thị từng member


                                                    //Lấy data user về
                                                    //


                                                }
                                            }).addOnFailureListener(new OnFailureListener() {

                                                @Override
                                                public void onFailure(@NonNull Exception exception) {
                                                    // Handle any errors
                                                }
                                            });
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        // Getting Post failed, log a message
                                        Log.w("e", "loadPost:onCancelled", databaseError.toException());
                                    }
                                };
                                UserInfoDAO.UserInfoServer.child(member.getUID()).addListenerForSingleValueEvent(userInfoListener);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            // Getting Post failed, log a message
                            Log.w("group", "loadPost:onCancelled", databaseError.toException());
                            // ...
                        }
                    };
                    GroupDAO.GroupDataServer.child(myGroup.GID).addListenerForSingleValueEvent(memberListener);

                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Getting Post failed, log a message
                    Log.w("group", "loadPost:onCancelled", databaseError.toException());
                    // ...
                }
            };
            UserInfoDAO.UserInfoServer.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addListenerForSingleValueEvent(myGroupListener);
        }

    }

    private void restoreVariable() {
        database = new Database(MapsActivity.this, "database.sqlite", null, 1);
        Cursor dataSavedPlace = database.GetData("select* from SavedPlace");
        int i = 1;
        while (dataSavedPlace.moveToNext()) {
            //Lấy dữ liệu từ database
            String id = dataSavedPlace.getString(0);
            String name = dataSavedPlace.getString(1);
            String note = dataSavedPlace.getString(2);
            String address = dataSavedPlace.getString(3);
            String AlarmTime = dataSavedPlace.getString(6);
            int IsChecked = parseInt(dataSavedPlace.getString(7));

            //Khởi tạo lại dữ liệu
            if (FormSavePlaceActivity.alarmManager == null)
                FormSavePlaceActivity.alarmManager = new ArrayList<>();
            if (FormSavePlaceActivity.arrayPendingIntent == null)
                FormSavePlaceActivity.arrayPendingIntent = new ArrayList<>();
            if (FormSavePlaceActivity.switch1 == null)
                FormSavePlaceActivity.switch1 = new ArrayList<>();
            if (FormSavePlaceActivity.calendars == null)
                FormSavePlaceActivity.calendars = new ArrayList<>();
            if (FormSavePlaceActivity.Name == null) FormSavePlaceActivity.Name = new ArrayList<>();
            if (FormSavePlaceActivity.Note == null) FormSavePlaceActivity.Note = new ArrayList<>();
            if (FormSavePlaceActivity.Time == null) FormSavePlaceActivity.Time = new ArrayList<>();
            if (FormSavePlaceActivity.IsChecked == null)
                FormSavePlaceActivity.IsChecked = new ArrayList<>();
            if (FormSavePlaceActivity.Id == null) FormSavePlaceActivity.Id = new ArrayList<>();
            if (FormSavePlaceActivity.TimeInstance == null)
                FormSavePlaceActivity.TimeInstance = new ArrayList<>();
            if (Sound.mediaPlayers == null) Sound.mediaPlayers = new ArrayList<>();
            if (FormSavePlaceActivity.Address == null) FormSavePlaceActivity.Address = new ArrayList<>();

            //Gán lại dữ liệu
            //Name Note Time IsChecked Id TimeInstance
            FormSavePlaceActivity.Name.add(name);
            FormSavePlaceActivity.Note.add(note);
            FormSavePlaceActivity.Time.add(AlarmTime);
            FormSavePlaceActivity.IsChecked.add(String.valueOf(IsChecked));
            FormSavePlaceActivity.Id.add(id);
            FormSavePlaceActivity.Address.add(address);
            Calendar calendar1 = Calendar.getInstance();
            int mDay = parseInt(AlarmTime.substring(0, 2));
            int mMonth = parseInt(AlarmTime.substring(3, 5));
            int mYear = parseInt(AlarmTime.substring(6, 10));
            int mHour = parseInt(AlarmTime.substring(11, 13));
            int mMinute = parseInt(AlarmTime.substring(14, 16));
            calendar1.set(Calendar.DAY_OF_MONTH, mDay);
            calendar1.set(Calendar.MONTH, mMonth - 1);
            calendar1.set(Calendar.YEAR, mYear - 1);
            calendar1.set(Calendar.HOUR_OF_DAY, mHour);
            calendar1.set(Calendar.MINUTE, mMinute);
            FormSavePlaceActivity.TimeInstance.add(calendar1);


            Intent intent1 = new Intent(this, AlarmReceiver.class);
            intent1.putExtra("name", name);
            intent1.putExtra("note", note);
            intent1.putExtra("address", address);
            intent1.putExtra("time", AlarmTime);
            intent1.putExtra("ischecked", IsChecked);
            intent1.putExtra("id", i);
            intent1.putExtra("extra", "on");
            intent1.putExtra("index", i - 1);
            PendingIntent pendingIntent;
            pendingIntent = PendingIntent.getBroadcast(this,
                    i - 1, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
            FormSavePlaceActivity.arrayPendingIntent.add(pendingIntent);
            FormSavePlaceActivity.alarmManager.add((AlarmManager) getSystemService(ALARM_SERVICE));
            Sound.mediaPlayers.add(MediaPlayer.create(this, R.raw.nhac));
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH, mDay);
            calendar.set(Calendar.MONTH, mMonth - 1);
            calendar.set(Calendar.YEAR, mYear);
            calendar.set(Calendar.HOUR_OF_DAY, mHour);
            calendar.set(Calendar.MINUTE, mMinute);
            FormSavePlaceActivity.calendars.add(calendar);
            FormSavePlaceActivity.switch1.add((Switch) findViewById(R.id.switch1));
            MapsActivity.number++;
            i++;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_setting:
                Toast.makeText(this, "Chức năng chưa phát triển!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_contact:
                //Toast.makeText(this, "Chức năng chưa phát triển!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, Contact.class);
                startActivity(intent);
                break;
            case R.id.nav_signout:
                if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                    userInfo.setDatabase(database);
                    if (!isNetworkConnected()) {
                        builder.setTitle("CẢNH BÁO!!!");
                        builder.setMessage("Do mất kết nối internet nên bạn có thể mất dữ liệu khi đăng xuất")
                                .setCancelable(true)
                                .setPositiveButton("Đăng xuất", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        NotSignInShow();
                                        userInfo.LogOut(getApplicationContext());
                                        Toast.makeText(getApplicationContext(), "Đã đăng xuất", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else {
                        NotSignInShow();
                        userInfo.LogOut(getApplicationContext());
                        Toast.makeText(getApplicationContext(), "Đã đăng xuất", Toast.LENGTH_SHORT).show();
                    }


                }
                break;
        }

        return true;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public void NotSignInShow() {
        Email.setVisibility(View.GONE);
        nameDisplay.setVisibility(View.GONE);
        btnEidtName.setVisibility(View.GONE);
        avatar.setVisibility(View.GONE);
        btnLogin.setVisibility(View.VISIBLE);
    }

    public void SignInShow() {
        Email.setVisibility(View.VISIBLE);
        nameDisplay.setVisibility(View.VISIBLE);
        btnEidtName.setVisibility(View.VISIBLE);
        avatar.setVisibility(View.VISIBLE);
        btnLogin.setVisibility(View.GONE);
    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }

    private void getNearByPlaces(String placeName) {
        url = getUrl(mLocation, placeName);
        dataTransfer[0] = mMap;
        dataTransfer[1] = url;
        getNearbyPlacesData.execute(dataTransfer);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(mContext, "onResume", Toast.LENGTH_SHORT).show();
        myGroup = new Group();
        loadMemberGroupLocation();
        startLocationUpdates();
        if (initMap) mMap.clear();
        dataTransfer = new Object[2];
        getNearbyPlacesData = new GetNearbyPlacesData();
        switch (action) {
            case 0:
                break;
            case 1:
                getNearByPlaces("atm");
                Toast.makeText(MapsActivity.this, "ATM gần đây!", Toast.LENGTH_LONG).show();
                break;
            case 2:
                getNearByPlaces("cafe");
                Toast.makeText(MapsActivity.this, "Cafe gần đây!", Toast.LENGTH_LONG).show();
                break;
            case 3:
                getNearByPlaces("gym");
                Toast.makeText(MapsActivity.this, "Gym gần đây!", Toast.LENGTH_LONG).show();
                break;
            case 4:
                getNearByPlaces("library");
                Toast.makeText(MapsActivity.this, "Thư viện gần đây!", Toast.LENGTH_LONG).show();
                break;
            case 5:
                getNearByPlaces("hospital");
                Toast.makeText(MapsActivity.this, "Bệnh viện gần đây!", Toast.LENGTH_LONG).show();
                break;
            case 6:
                getNearByPlaces("pharmacy");
                Toast.makeText(MapsActivity.this, "Nhà thuốc gần đây!", Toast.LENGTH_LONG).show();
                break;
            case 7:
                getNearByPlaces("restaurant");
                Toast.makeText(MapsActivity.this, "Nhà hàng gần đây!", Toast.LENGTH_LONG).show();
                break;
            case 8:
                getNearByPlaces("school");
                Toast.makeText(MapsActivity.this, "Trường học gần đây!", Toast.LENGTH_LONG).show();
                break;
            case 9:
                getNearByPlaces("supermarket");
                Toast.makeText(MapsActivity.this, "Trung tâm mua sắm gần đây!", Toast.LENGTH_LONG).show();
                break;
            case 10:
                getNearByPlaces("bakery");
                Toast.makeText(MapsActivity.this, "Tiệm bánh gần đây!", Toast.LENGTH_LONG).show();
                break;
            case 11:
                getNearByPlaces("busstation");
                Toast.makeText(MapsActivity.this, "Trạm xe buýt gần đây!", Toast.LENGTH_LONG).show();
                break;
            case 12:
                getNearByPlaces("church");
                Toast.makeText(MapsActivity.this, "Nhà thờ gần đây!", Toast.LENGTH_LONG).show();
                break;
            case 13:
                getNearByPlaces("dentist");
                Toast.makeText(MapsActivity.this, "Nha khoa gần đây!", Toast.LENGTH_LONG).show();
                break;
            case 14:
                getNearByPlaces("spa");
                Toast.makeText(MapsActivity.this, "Spa gần đây!", Toast.LENGTH_LONG).show();
                break;
            case 15:
                getNearByPlaces("shoes");
                Toast.makeText(MapsActivity.this, "Cửa hàng giày dép gần đây!", Toast.LENGTH_LONG).show();
                break;
        }
        //Set lại action = 0 để mỗi khi resume ko thực hiện chức năng tìm địa điểm gần đây
        action=0;

        builder2 = new AlertDialog.Builder(this);
        NotSignInShow();
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            SignInShow();
            avatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, Request_code2);
                }
            });
            //Thay đổi tên
            final String nameDefult = splitEmail(Email.getText().toString());
            nameDisplay.setText(nameDefult);
            editName = nav_header.findViewById(R.id.edit_name);
            editName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final EditText input = new EditText(MapsActivity.this);
                    builder2.setView(input);
                    builder2.setTitle("Thay đổi tên của bạn")
                            .setCancelable(true)
                            .setPositiveButton("Hủy", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton("Lưu", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    nameDisplay.setText(input.getText().toString());
                                    UserInfoDAO.UpdateNameDisplay(userInfo.getUID(), nameDisplay.getText().toString());
                                }
                            });
                    AlertDialog alert = builder2.create();
                    alert.show();
                }
            });

            //Load avatar, displayname về
            userInfo = new UserInfo();
            userInfo = UserInfoDAO.GetCurrentUserInfo();
            if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                FirebaseStorage storage = FirebaseStorage.getInstance();
                String fileName = "avatar.jpg";
                String Path = userInfo.getUID() + "/" + fileName;

                StorageReference storageRef = storage.getReference(Path);

                final File localFile;
                try {

                    File tempFile = new File("avatar.jpg");
                    boolean exists = tempFile.exists();
                    if (exists == true) {
                        tempFile.delete();
                    }
                    localFile = File.createTempFile("avatar", "jpg");
                    storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                            // Local temp file has been created
                            Uri avatar_Uri = Uri.fromFile(localFile);
                            avatarUri = avatar_Uri;
                            avatar.setImageURI(avatar_Uri);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle any errors
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }


                ValueEventListener userInfoListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get Post object and use the values to update the UI
                        try {
                            nameDisplay.setText(dataSnapshot.child(UserInfoDAO.DisplayNameRef).getValue().toString());
                            Email.setText(dataSnapshot.child(UserInfoDAO.EmailRef).getValue().toString());

                            userInfo.setDisplayName(nameDisplay.getText().toString());
                            userInfo.setEmail(Email.getText().toString());
                        } catch (Exception e) {

                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Getting Post failed, log a message
                        Log.w("e", "loadPost:onCancelled", databaseError.toException());

                    }
                };


                UserInfoDAO.UserInfoServer.child(userInfo.getUID()).addListenerForSingleValueEvent(userInfoListener);


            }

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FindPlaceInList.RETURN_CODE) {
            action = data.getIntExtra("num", -1);
        }
        if (requestCode == Request_code2 && resultCode == RESULT_OK && data != null) {
            avatarUri = data.getData();
            avatar.setImageURI(avatarUri);
            if (FirebaseAuth.getInstance().getCurrentUser() != null) {

                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageRef = storage.getReference();

                String fileName = "avatar.jpg";

                String Path = FirebaseAuth.getInstance().getCurrentUser().getUid() + "/" + fileName;
                Toast.makeText(getApplicationContext(), Path, Toast.LENGTH_SHORT).show();

                StorageReference UserID_Folder = storageRef.child(Path);


                StorageMetadata metadata = new StorageMetadata.Builder()
                        .setContentType("image/jpg")
                        .build();


                UserID_Folder.putFile(avatarUri, metadata)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                // Get a URL to the uploaded content
                                //Uri downloadUrl = taskSnapshot.getUploadSessionUri();
                                //result
                                Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Handle unsuccessful uploads
                                // ...
                                Toast.makeText(getApplicationContext(), exception.getCause().getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
            }
        }
        if (requestCode == 123 && resultCode == Activity.RESULT_OK) {
            fragment = new SavedPlaceFragment();
            loadFragment(fragment);
            Toast.makeText(this, "Lưu thành công", Toast.LENGTH_SHORT).show();
        }

    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Request_code);
            }
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        initMap = true;
        //  LƯU ĐỊA ĐIỂM - HIỂN THỊ DIALOG
        builder = new AlertDialog.Builder(this);
        mMap = googleMap;
        mMap.clear();
        //THÊM ĐỊA ĐIỂM LOCATION

        LatLng latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
        final MarkerOptions[] markerOptions = {new MarkerOptions().position(latLng).title("Vị trí của tôi")};
        //chèn avatar location
        markerOptions[0].icon(BitmapDescriptorFactory.fromBitmap(createCustomMarker(MapsActivity.this,
                avatarUri, "VH")));
        CameraPosition googlePlex = CameraPosition.builder()
                .target(latLng)
                .zoom(16)
                .bearing(0)
                .tilt(45)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 10, null);
        if (mkLocation != null) {
            mkLocation.remove();
        }
        mkLocation = mMap.addMarker(markerOptions[0]);

//            if (mkMem1!=null){
//                mkMem1.remove();
//            }
//            LatLng mem1 = new LatLng(10.7671296240577144, 106.66341293603182);
//            mkMem1 = mMap.addMarker(new MarkerOptions().position(mem1).title("Hồ Hiếu")
//                .icon(BitmapDescriptorFactory.fromBitmap(createCustomMarker(MapsActivity.this,
//                        mem1Uri, "HH"))));
        mMap.setMyLocationEnabled(true);
        int key = getIntent().getIntExtra("keyID", 1234);
        if (key == 12598) {//neu activity duoc goi tu adapter
            Intent myCallerIntent = getIntent();
            Bundle myBundle = myCallerIntent.getExtras();
            final Double latitude = Double.parseDouble(myBundle.getString("latitude"));
            final Double longitude = Double.parseDouble(myBundle.getString("longitude"));
            LatLng latLng1 = new LatLng(latitude, longitude);
            MarkerOptions markerOptions1 = new MarkerOptions().position(latLng1).
                    title(myBundle.getString("name"));
            CameraPosition googlePlex1 = CameraPosition.builder().target(latLng1).zoom(16).bearing(0).tilt(45).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex1), 10, null);
            mMap.addMarker(markerOptions1);
            mMap.setMyLocationEnabled(true);
        }
        //BẮT SỰ KIỆN LONGCLICK VÀO MAP
        final Marker[] longClickMarker = new Marker[1];
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(final LatLng latLng) {
                if (longClickMarker[0] != null) {
                    longClickMarker[0].remove();
                }
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng)
                        .title(latLng.latitude + " : " + latLng.longitude)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                longClickMarker[0] = mMap.addMarker(markerOptions);
                Geocoder geocoder;
                List<Address> addresses = null;
                geocoder = new Geocoder(getApplication(), Locale.getDefault());
                final double latitude = latLng.latitude, longitude = latLng.longitude;
                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (addresses != null) {
                    final String address = addresses.get(0).getAddressLine(0);
                    builder.setMessage("Bạn có muốn lưu lại địa chỉ này không?")
                            .setCancelable(true)
                            .setPositiveButton("Lưu", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    number++;
                                    Intent i = new Intent();
                                    i.setClass(MapsActivity.this, FormSavePlaceActivity.class);
                                    Intent myIntentA1A2 = new Intent(MapsActivity.this, FormSavePlaceActivity.class);
                                    Bundle myBundle1 = new Bundle();
                                    myBundle1.putString("address", address);
                                    myBundle1.putDouble("longitude", longitude);
                                    myBundle1.putDouble("latitude", latitude);
                                    myBundle1.putInt("index", number - 1);
                                    myBundle1.putString("key", "Create");
                                    myIntentA1A2.putExtras(myBundle1);
                                    startActivity(myIntentA1A2);
                                }
                            })
                            .setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.setTitle(address);
                    alert.show();
                } else {
                    Toast.makeText(MapsActivity.this, "Không lấy được địa chỉ", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //ĐÓNG ỨNG DỤNG
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    Fragment fragment = null;
    //BẮT SỰ KIỆN THANH MENU BÊN DƯỚI
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_place_saved:
                    fragment = new SavedPlaceFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_home:
                    if (fragment != null) {
                        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                    }
                    onResume();

                    return true;
                case R.id.navigation_group:
                    if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                        fragment = new GroupFragment();
                        loadFragment(fragment);
                        return true;
                    } else {
                        Toast.makeText(MapsActivity.this, "Bạn cần đăng nhập để sử dụng chức năng này", Toast.LENGTH_LONG).show();
                        return false;
                    }
            }
            return false;
        }
    };

    //THÊM FRAGMENT
    public void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container_frame, fragment);
        transaction.commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Request_code:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(getApplicationContext(), "Ứng dụng cần được cấp quyền truy cập vị trí", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
        }
    }

    int PROXIMITY_RADIUS = 10000;

    private String getUrl(Location location, String nearbyPlace) {

        StringBuilder googlePlaceUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlaceUrl.append("location=" + location.getLatitude() + "," + location.getLongitude());
        googlePlaceUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlaceUrl.append("&type=" + nearbyPlace);
        googlePlaceUrl.append("&sensor=true");
        googlePlaceUrl.append("&key=" + "AIzaSyC4u_yYqLuclbOjbih69NG0H1_gqajEdwE");

        Log.d("MapsActivity", "url = " + googlePlaceUrl.toString());
        return googlePlaceUrl.toString();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public String splitEmail(String s) {
        String[] output = s.split("@");
        return output[0];
    }

    public static Bitmap createCustomMarker(Context context, Uri avatarUri, String _name) {

        View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);

        CircleImageView markerImage = marker.findViewById(R.id.mapAvatar);
        markerImage.setImageURI(avatarUri);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        marker.setLayoutParams(new ViewGroup.LayoutParams(50, ViewGroup.LayoutParams.WRAP_CONTENT));
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        marker.draw(canvas);

        return bitmap;
    }


}

