package com.example.mymap;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymap.UserData.SavedPlaceDAO;

import java.text.Normalizer;
import java.util.Calendar;
import java.util.List;

import static android.content.Context.ALARM_SERVICE;

public class SavedPlaceAdapter extends RecyclerView.Adapter<SavedPlaceAdapter.ViewHolder>{

    Context context;
    public Database db;
    private int layout;
    private List<SavedPlace> savedPlaceList;

    public SavedPlaceAdapter(Context context, int layout, List<SavedPlace> savedPlaceList) {
        this.context = context;
        this.layout = layout;
        this.savedPlaceList = savedPlaceList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layout,parent,false );
        return  new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final int tmp=position;
        final SavedPlace savedPlace = savedPlaceList.get(position);
        db=new Database(context,"database.sqlite",null,1);
        holder.name.setText(savedPlace.getName());
        holder.address.setText(savedPlace.getAddress());
        holder.alarmTime.setText(savedPlace.getAlarmTime());
        if (savedPlace.getIsChecked()==1) holder.state.setText("Trạng thái: Bật");
        else holder.state.setText("Trạng thái: Tắt");

        holder.txtOptionDigit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(context,holder.txtOptionDigit);
                popupMenu.inflate(R.menu.menu_saved_fragment);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch(item.getItemId()){
                            case R.id.Note:
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Ghi chú")
                                        .setCancelable(true);
                                builder.setMessage(savedPlace.getNote());
                                AlertDialog alert = builder.create();
                                alert.show();
                                break;
                            case R.id.MoveToMap:
                                Intent myIntentA1A2=new Intent(context,MapsActivity.class);
                                myIntentA1A2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                myIntentA1A2.putExtra("longitude", savedPlace.getLongitude());
                                myIntentA1A2.putExtra("latitude", savedPlace.getLatitude());
                                myIntentA1A2.putExtra("name", savedPlace.getName());
                                myIntentA1A2.putExtra("keyID",12598);
                                context.startActivity(myIntentA1A2);
                                break;
                            case R.id.EditPlaceSaved:
                                Intent myIntentA1A2_2=new Intent(context,FormSavePlaceActivity.class);
                                Bundle myBundle1=new Bundle();
                                myBundle1.putString("key","Edit");//key dung de phan biet la edit hay tao moi
                                myBundle1.putString("id", savedPlace.getId());
                                myBundle1.putString("name", savedPlace.getName());
                                myBundle1.putString("address", savedPlace.getAddress());
                                myBundle1.putString("note", savedPlace.getNote());
                                myBundle1.putString("time", savedPlace.getAlarmTime());
                                myBundle1.putString("latitude", savedPlace.getLatitude());
                                myBundle1.putString("longitude", savedPlace.getLongitude());
                                myBundle1.putInt("check",savedPlace.getIsChecked());
                                myBundle1.putInt("position",tmp);
                                myIntentA1A2_2.putExtras(myBundle1);
                                ((Activity)context).startActivityForResult(myIntentA1A2_2,123);
                                break;
                            case R.id.RemovePlacecSaved:
                                removeView(tmp);
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }
    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return savedPlaceList.size();
    }

    public class ViewHolder extends RecyclerView .ViewHolder{
        TextView name,address,alarmTime,txtOptionDigit,state;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.namePlaceSaved);
            address=itemView.findViewById(R.id.addressPlaceSaved);
            alarmTime=itemView.findViewById(R.id.alarmTimePlaceSaved);
            txtOptionDigit=itemView.findViewById(R.id.txtOptionDigit);
            state= itemView.findViewById(R.id.textViewState);
        }
    }

    public void removeView(int position)
    {
        final int positiontmp=position;
        final String pos= savedPlaceList.get(position).getId();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Xóa địa điểm " + savedPlaceList.get(positiontmp).getName() + "?")
                .setCancelable(true)
                .setIcon(R.drawable.ic_location)
                .setPositiveButton("Xóa", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Xóa vị trí positiontmp trong list savedplace
                        savedPlaceList.remove(positiontmp);
                        for(int i=positiontmp;i < savedPlaceList.size();i++)
                            savedPlaceList.get(i).setId(String.valueOf(i+1));

                        //Xóa hàng có id=pos trong database local
                        db.QueryData("DELETE FROM SavedPlace WHERE Id='"+pos+"';");
                        db.QueryData("DELETE FROM SQLITE_SEQUENCE WHERE Name='SavedPlace'");
                        //Update id về lại 1 2 3 4 5 ...
                        db.UpdateIDSavedPlace();

                        //Nhận diện sự thay đổi
                        SavedPlaceDAO savedPlaceDAO=new SavedPlaceDAO(db);
                        savedPlaceDAO.PushSavedPlaceData();
                        notifyDataSetChanged();


                        Intent intent=new Intent(context,AlarmReceiver.class);
                        //Hủy những báo thức phía sau
                        for(int i=positiontmp+1;i < MapsActivity.number;i++){
                            FormSavePlaceActivity.alarmManager.get(i).cancel(FormSavePlaceActivity.arrayPendingIntent.get(i));
                        }
                        //Hủy alarm manager ngay vị trí xóa và xóa mediaPlayers,switch1,arrayPendingIntent của vị trí đó
                        Intent intent1=new Intent(context, AlarmReceiver.class);
                        FormSavePlaceActivity.alarmManager.get(positiontmp).cancel
                                (FormSavePlaceActivity.arrayPendingIntent.get(positiontmp));
                        Sound.mediaPlayers.remove(positiontmp);
                        FormSavePlaceActivity.alarmManager.remove(positiontmp);
                        FormSavePlaceActivity.switch1.remove(positiontmp);
                        FormSavePlaceActivity.arrayPendingIntent.remove(positiontmp);
                        FormSavePlaceActivity.calendars.remove(positiontmp);
                        FormSavePlaceActivity.TimeInstance.remove(positiontmp);
                        FormSavePlaceActivity.Name.remove(positiontmp);
                        FormSavePlaceActivity.Note.remove(positiontmp);
                        FormSavePlaceActivity.Time.remove(positiontmp);
                        FormSavePlaceActivity.Address.remove(positiontmp);
                        FormSavePlaceActivity.IsChecked.remove(positiontmp);
                        FormSavePlaceActivity.Id.remove(positiontmp);
                        MapsActivity.number--;
                        //Khởi động lại những báo thức sau vị trí xóa
                        for(int i=positiontmp;i<MapsActivity.number;i++){
                            Intent intent2=new Intent(context,AlarmReceiver.class);
                            FormSavePlaceActivity.Id.set(i,(Integer.parseInt(FormSavePlaceActivity.Id.get(i))-1) +"");
                            if(Integer.parseInt(FormSavePlaceActivity.IsChecked.get(i))==1){
                                intent2.putExtra("name",FormSavePlaceActivity.Name.get(i));
                                intent2.putExtra("note",FormSavePlaceActivity.Note.get(i));
                                intent2.putExtra("time",FormSavePlaceActivity.Time.get(i));
                                intent2.putExtra("address",FormSavePlaceActivity.Address.get(i));
                                intent2.putExtra("ischecked",Integer.parseInt(FormSavePlaceActivity.IsChecked.get(i)));
                                intent2.putExtra("id",Integer.parseInt(FormSavePlaceActivity.Id.get(i)));
                                intent2.putExtra("extra","on");
                                intent2.putExtra("index",i);
                                PendingIntent pendingIntent;
                                pendingIntent= PendingIntent.getBroadcast(context,
                                        i,intent2,PendingIntent.FLAG_UPDATE_CURRENT);
                                FormSavePlaceActivity.arrayPendingIntent.set(i,pendingIntent);
                                FormSavePlaceActivity.alarmManager.set(i,(AlarmManager) context.getSystemService(ALARM_SERVICE));
//                                if (FormSavePlaceActivity.calendars.get(i).getTimeInMillis() >
//                                        FormSavePlaceActivity.TimeInstance.get(i).getTimeInMillis()
//                                        && savedPlaceList.get(i-1).getIsChecked() == 1)
                                if (FormSavePlaceActivity.calendars.get(i).getTimeInMillis() >
                                        FormSavePlaceActivity.TimeInstance.get(i).getTimeInMillis()
                                        && savedPlaceList.get(i).getIsChecked() == 1){
                                FormSavePlaceActivity.alarmManager.get(i).set(AlarmManager.RTC_WAKEUP,
                                        FormSavePlaceActivity.calendars.get(i).getTimeInMillis(),
                                        FormSavePlaceActivity.arrayPendingIntent.get(i));
                                Log.e("On","Đã bật âm báo " + i);
                                }
                            }
                        }
                        Toast.makeText(context, "Đã xóa", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
