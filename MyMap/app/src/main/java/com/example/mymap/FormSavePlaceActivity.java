package com.example.mymap;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.mymap.UserData.SavedPlaceDAO;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;

public class FormSavePlaceActivity extends AppCompatActivity {
    Toolbar mToolbar;
    Button btnDatePicker, btnTimePicker,submit;
    TextView txtDate, txtTime,title;
    int mYear=-1, mMonth=-1, mDay=-1, mHour=-1, mMinute=-1;
    EditText name,note;
    String id;
    Database db;
    String address;
    int isChecked=0;
    BroadcastReceiver broadcastReceiver=new AlarmReceiver();
    Double latitude,longitude;


    Calendar calendar=Calendar.getInstance();
    public static ArrayList<AlarmManager> alarmManager;
    public static ArrayList<PendingIntent> arrayPendingIntent;
    public static ArrayList<Switch> switch1;//Switch trong FormSavePlace
    public static ArrayList<Calendar> calendars;
    public static ArrayList<String> Name;
    public static ArrayList<String> Note;
    public static ArrayList<String> Time;
    public static ArrayList<String> Address;
    public static ArrayList<String> IsChecked;
    public static ArrayList<String> Id;
    public static ArrayList<Calendar> TimeInstance;
    int index=0;
    PendingIntent pendingIntent;
    long interval=40000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_save_place);
        if(alarmManager==null) alarmManager=new ArrayList<>();
        if (arrayPendingIntent==null) arrayPendingIntent=new ArrayList<>();
        if(switch1==null) switch1=new ArrayList<>();
        if(calendars==null) calendars=new ArrayList<>();
        if(Name==null) Name=new ArrayList<>();
        if(Note==null) Note=new ArrayList<>();
        if(Time==null) Time=new ArrayList<>();
        if(Address==null) Address=new ArrayList<>();
        if(IsChecked==null) IsChecked=new ArrayList<>();
        if(Id==null) Id=new ArrayList<>();
        if(TimeInstance==null) TimeInstance=new ArrayList<>();
        db=new Database(this,"database.sqlite",null,1);
        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch1.remove(index);
                MapsActivity.number--;
                finish();
            }
        });
        final String key=getIntent().getStringExtra("key");
        if (key.equals("Create")) {
            Intent myCallerIntent = getIntent();
            Bundle myBundle = myCallerIntent.getExtras();
            index=myBundle.getInt("index");
        }
        if (key.equals("Edit")) {
            Intent myCallerIntent = getIntent();
            Bundle myBundle = myCallerIntent.getExtras();
            index=myBundle.getInt("position");
        }
        init();
        if (key.equals("Create")) {
            Intent myCallerIntent = getIntent();
            Bundle myBundle = myCallerIntent.getExtras();
            address = myBundle.getString("address");
            latitude = myBundle.getDouble("latitude");
            longitude = myBundle.getDouble("longitude");
        }
        if (key.equals("Edit")) {
            Intent myCallerIntent = getIntent();
            Bundle myBundle = myCallerIntent.getExtras();
            address = myBundle.getString("address");
            String nameReceive = myBundle.getString("name");
            String noteReceive = myBundle.getString("note");
            String timeReceive = myBundle.getString("time");
            String latitudeReceive = myBundle.getString("latitude");
            String longitudeReceive = myBundle.getString("longitude");
            String idReceive=myBundle.getString("id");
            index=myBundle.getInt("position");
            latitude=parseDouble(latitudeReceive);
            longitude=parseDouble(longitudeReceive);
            isChecked=myBundle.getInt("check");
            if(isChecked==1) {
                if(switch1.size() <= index){
                    Switch switch3 = new Switch(this);
                    switch3.setChecked(true);
                    switch1.add(switch3);
                }else switch1.get(index).setChecked(true);
            }
            else {
                if (switch1.size() <= index){
                    Switch switch3 = new Switch(this);
                    switch3.setChecked(false);
                    switch1.add(switch3);

                }else switch1.get(index).setChecked(false);
            }
            id=idReceive;
            setTextDateAndTime(timeReceive);
            name.setText(nameReceive);
            note.setText(noteReceive);
        }

        DateTimePicker();
        title.setText(address);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDay == -1 || mMinute == -1 || name.getText().toString().trim().length() == 0) {
                    Toast.makeText(FormSavePlaceActivity.this, "Thông tin chưa chính xác", Toast.LENGTH_SHORT).show();
                } else {
                    final Intent intent1=new Intent(FormSavePlaceActivity.this, AlarmReceiver.class);
                    if(switch1.get(index).isChecked())isChecked=1;else isChecked=0;
                    String time = formatDateTime(mDay, mMonth, mYear, mHour, mMinute);
                    intent1.putExtra("name",name.getText().toString());
                    intent1.putExtra("note",note.getText().toString());
                    intent1.putExtra("address",address);
                    intent1.putExtra("time",time);
                    intent1.putExtra("ischecked",isChecked);
//                    intent1.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                    intent1.setAction(MapsActivity.MY_ACTION);
                    Log.e("Submit","Đang trong Submit");
                    Log.e("Index = ",index+"");
                    //Nếu tạo mới
                    if (key.equals("Create")) {
                        int newID = db.CountSavedPlace() + 1;
                        intent1.putExtra("id",newID);
                        intent1.putExtra("extra","on");
                        intent1.putExtra("index",index);
                        pendingIntent=PendingIntent.getBroadcast(FormSavePlaceActivity.this,
                                index,intent1,PendingIntent.FLAG_UPDATE_CURRENT);
                        arrayPendingIntent.add(pendingIntent);
                        if(alarmManager.size() <= index)
                            alarmManager.add((AlarmManager)getSystemService(ALARM_SERVICE));
                        else alarmManager.set(index,(AlarmManager)getSystemService(ALARM_SERVICE));
                        //Khởi tạo mediaPlayers
                        if (Sound.mediaPlayers==null)
                            Sound.mediaPlayers=new ArrayList<>();
                        if (Sound.mediaPlayers.size() <= index)
                            Sound.mediaPlayers.add(MediaPlayer.create(FormSavePlaceActivity.this,R.raw.nhac));
                        calendar.set(Calendar.DAY_OF_MONTH,mDay);
                        calendar.set(Calendar.MONTH,mMonth-1);
                        calendar.set(Calendar.YEAR,mYear);
                        calendar.set(Calendar.HOUR_OF_DAY,mHour);
                        calendar.set(Calendar.MINUTE,mMinute);
                        calendars.add(calendar);
                        Name.add(name.getText().toString());
                        Note.add(note.getText().toString());
                        Time.add(time);
                        Address.add(address);
                        IsChecked.add(String.valueOf(isChecked));
                        Calendar calendarInstance=Calendar.getInstance();
                        TimeInstance.add(calendarInstance);
                        //Nếu muốn mở thông báo
                        if (switch1.get(index).isChecked()){
                            if (calendar.getTimeInMillis() >= calendarInstance.getTimeInMillis()){
                                alarmManager.get(index).set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),arrayPendingIntent.get(index));
                                Log.e("On","Đã bật âm báo " + index);
                                Toast.makeText(FormSavePlaceActivity.this, "Đã bật âm báo địa điểm " + name.getText().toString(), Toast.LENGTH_SHORT).show();
                            }else  Toast.makeText(FormSavePlaceActivity.this, "Hãy đặt thời gian lớn hơn thời điểm hiện tại", Toast.LENGTH_SHORT).show();
                        }
                        else{//Nếu muốn tắt thông báo
                            //Không làm gì cả
                        }
                        //Thêm 1 dòng SavedPlace mới vào bảng SavedPlace va day du lieu len server
                        SavedPlace savedPlace=new SavedPlace("",name.getText().toString(),note.getText().toString(),address,""+latitude,
                                ""+longitude,time,isChecked);
                        db.InsertSavedPlace(savedPlace);
                        Id.add(String.valueOf(newID));
                        savedPlace.setId(""+newID);
                        try {SavedPlaceDAO.UploadData(savedPlace);
                        } catch (Exception e) {e.printStackTrace();}
                        if (!isNetworkConnected()) {
                            Toast.makeText(FormSavePlaceActivity.this, "Kết nối mạng không ổn định", Toast.LENGTH_LONG).show();
                        }
                        finish();
                    }
                    //Nếu đang chỉnh sửa
                    if (key.equals("Edit")) {
                        intent1.putExtra("id",Integer.parseInt(id));
                        //Nếu muốn mở thông báo
                        if (switch1.get(index).isChecked()){
                            intent1.putExtra("extra","on");
                            intent1.putExtra("index",index);
                            pendingIntent=PendingIntent.getBroadcast(FormSavePlaceActivity.this,
                                    index,intent1,PendingIntent.FLAG_UPDATE_CURRENT);
                            arrayPendingIntent.set(index,pendingIntent);
                            calendar.set(Calendar.DAY_OF_MONTH,mDay);
                            calendar.set(Calendar.MONTH,mMonth-1);
                            calendar.set(Calendar.YEAR,mYear);
                            calendar.set(Calendar.HOUR_OF_DAY,mHour);
                            calendar.set(Calendar.MINUTE,mMinute);
                            alarmManager.set(index,(AlarmManager)getSystemService(ALARM_SERVICE));
                            Calendar calendarInstance=Calendar.getInstance();
                            TimeInstance.set(index,calendarInstance);
                            calendars.set(index,calendar);
                            Name.set(index,name.getText().toString());
                            Note.set(index,note.getText().toString());
                            Time.set(index,time);
                            Address.set(index,address);
                            IsChecked.set(index,String.valueOf(isChecked));
                            Id.set(index,String.valueOf(id));
                            if (calendar.getTimeInMillis() >= calendarInstance.getTimeInMillis())
                            {
                                alarmManager.get(index).set(AlarmManager.RTC_WAKEUP,
                                        calendar.getTimeInMillis(),arrayPendingIntent.get(index));
                                Toast.makeText(FormSavePlaceActivity.this, "Đã bật âm báo địa điểm " + name.getText().toString(), Toast.LENGTH_SHORT).show();
                                Log.e("On","Đã bật âm báo " + index);
                            }else
                                Toast.makeText(FormSavePlaceActivity.this, "Hãy đặt thời gian lớn hơn thời điểm hiện tại", Toast.LENGTH_SHORT).show();

                        }else{//Nếu muốn tắt thông báo
                            alarmManager.get(index).cancel(arrayPendingIntent.get(index));
                            Toast.makeText(FormSavePlaceActivity.this, "Đã tắt âm báo địa điểm " + name.getText().toString(), Toast.LENGTH_SHORT).show();
                            intent1.putExtra("extra","off");
                            intent1.putExtra("extra1","cancel");
                            intent1.putExtra("index",index);
                            sendBroadcast(intent1);
                        }
                        Id.set(index,id);
                        db.QueryData("UPDATE SavedPlace set name='"+name.getText().toString()
                                +"',note='"+note.getText().toString()+"',AlarmTime='"+time+"',IsChecked="+isChecked+" WHERE id='"+id+"'");
                        try {
                            SavedPlace savedPlace=new SavedPlace(""+id,name.getText().toString(),note.getText().toString(),address,""+latitude,
                                    ""+longitude,time,isChecked);
                            SavedPlaceDAO.UploadData(savedPlace);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (!isNetworkConnected()) {
                            Toast.makeText(FormSavePlaceActivity.this, "Kết nối mạng không ổn định", Toast.LENGTH_LONG).show();
                        }
                        Intent tmp=new Intent();
                        setResult(Activity.RESULT_OK,tmp);
                        finish();
                    }
                }
            }
        });
    }

    public void init()
    {
        submit=findViewById(R.id.submit);
        btnDatePicker=findViewById(R.id.datePicker);
        btnTimePicker=findViewById(R.id.timePicker);
        txtDate=findViewById(R.id.inDate);
        txtTime=findViewById(R.id.inTime);
        //Nếu chưa khởi tạo
        if (switch1.size() <= index) switch1.add((Switch) findViewById(R.id.switch1));
        else switch1.set(index, (Switch) findViewById(R.id.switch1));
        name=findViewById(R.id.name1);
        note=findViewById(R.id.note1);
        title=findViewById(R.id.title1);
    }
    private void DateTimePicker()
    {
        btnDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                DatePickerDialog datePickerDialog =
                        new DatePickerDialog(FormSavePlaceActivity.this, new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                                calendar.set(i, i1, i2);
                                mYear=i;mMonth=i1+1;mDay=i2;
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                txtDate.setText(simpleDateFormat.format(calendar.getTime()));
                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
                datePickerDialog.show();
            }
        });
        btnTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                TimePickerDialog timePickerDialog = new TimePickerDialog(FormSavePlaceActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,int minute) {
                                mHour =hourOfDay;mMinute = minute;
                                txtTime.setText(hourOfDay+":"+minute);
                            }
                        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);
                timePickerDialog.show();
            }
        });
    }
    public void setTextDateAndTime(String timeReceive)
    {
        String date=timeReceive.substring(0,10);
        txtDate.setText(date);
        String time=timeReceive.substring(11,16);
        txtTime.setText(time);
        mDay=parseInt(timeReceive.substring(0,2));
        mMonth=parseInt(timeReceive.substring(3,5));
        mYear=parseInt(timeReceive.substring(6,10));
        mHour=parseInt(timeReceive.substring(11,13));
        mMinute=parseInt(timeReceive.substring(14,16));
    }
    private  String  formatDateTime(int day,int month,int year,int hour,int min)
    {
        String result="";
            if (day<10) result+="0"+day; else  result+=""+day;
            if (month<10) result+="/0"+month; else  result+="/"+month;
            result+="/"+year+" ";
            if (hour<10) result+="0"+hour; else  result+=""+hour;
            if (min<10) result+=":0"+min; else  result+=":"+min;
           return result;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        switch1.remove(index);
        MapsActivity.number--;
    }
}

