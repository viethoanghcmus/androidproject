package com.example.mymap.UserInfo;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.example.mymap.Database;
import com.example.mymap.Login.Login;
import com.example.mymap.MapsActivity;
import com.example.mymap.UserData.SavedPlaceDAO;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class UserInfo {
    private Database database;
    String UID;
    String DisplayName;
    Uri UriAvatar;
    String Email;

    public UserInfo(String UID, String displayName, Uri uriAvatar, String email) {
        this.UID = UID;
        DisplayName = displayName;
        UriAvatar = uriAvatar;
        Email = email;
    }

    public Database getDatabase() {
        return database;
    }

    public void setDatabase(Database database) {
        this.database = database;
    }

    public UserInfo() {
        UID = "";
        DisplayName = "";
        UriAvatar = null;
        Email = "";
    }
    //Khởi tạo User
    //Thành công trả về bool
    public boolean Init() {

        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            FirebaseUser fireUser = FirebaseAuth.getInstance().getCurrentUser();
            UID = fireUser.getUid();
            DisplayName = fireUser.getDisplayName();
            UriAvatar = fireUser.getPhotoUrl();
            Email = fireUser.getEmail();
            return true;
        }
        return false;
    }

    public void ShowInfo() {

    }

    public void LogOut(Context context) {
        //Xóa kêu tất cả các alarm và renew các biến
        Login.DeleteAllAlarm();
        //xoa database, push du lieu len server truoc khi dang xuat
        SavedPlaceDAO savedPlaceDAO= new SavedPlaceDAO(database);
        savedPlaceDAO.PushSavedPlaceData();
        database.ClearTableSavedPlace();
        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseAuth.signOut();
        //Mở sang màn hình home
        MapsActivity mapsActivity=new MapsActivity();
        Intent i=new Intent(context,mapsActivity.getClass());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    public String getUID() {
        return UID;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public Uri getUriAvatar() {
        return UriAvatar;
    }

    public String getEmail() {
        return Email;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public void setUriAvatar(Uri uriAvatar) {
        UriAvatar = uriAvatar;
    }

    public void setEmail(String email) {
        Email = email;
    }




}
