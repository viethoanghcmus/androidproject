package com.example.mymap;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PlacesAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<Places> placesList;
    //private ArrayList<Places> arrayList;

    public PlacesAdapter(Context context, int layout, List<Places> placesList) {
        this.context = context;
        this.layout = layout;
        this.placesList = placesList;
        //this.arrayList = new ArrayList<Places>();
        //this.arrayList.addAll(placesList);
    }

    @Override
    public int getCount() {
        return placesList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public class ViewHolder{
        ImageView imgRepresent;
        TextView txtName;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view==null){
            LayoutInflater layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=layoutInflater.inflate(layout,null);
            holder=new ViewHolder();
            //Ánh xạ
            holder.imgRepresent=(ImageView)view.findViewById(R.id.imageviewRepresent);
            holder.txtName=(TextView) view.findViewById(R.id.textviewName);
            view.setTag(holder);
        }else{
            holder= (ViewHolder) view.getTag();
        }
        Places places=placesList.get(i);
        holder.imgRepresent.setImageResource(places.getIcon());
        holder.txtName.setText(places.getName());
        return view;
    }

//    public void filter(String charText) {
//        charText = charText.toLowerCase(Locale.getDefault());
//        placesList.clear();
//        if (charText.length() == 0) {
//            placesList.addAll(arrayList);
//        } else {
//            for (Places wp : arrayList) {
//                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
//                    placesList.add(wp);
//                }
//            }
//        }
//        notifyDataSetChanged();
//    }
}
