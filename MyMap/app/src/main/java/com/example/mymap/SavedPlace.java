package com.example.mymap;

public class SavedPlace {
        private String id;
        private  String name;
        private  String note;
        private  String address;
        private  String latitude;
        private  String longitude;
        private String AlarmTime;
        private int IsChecked;

    public SavedPlace(String id, String name, String note, String address, String latitude, String longitude, String alarmTime, int IsChecked) {
        this.id = id;
        this.name = name;
        this.note = note;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.AlarmTime = alarmTime;
        this.IsChecked=IsChecked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAlarmTime() {
        return AlarmTime;
    }

    public void setAlarmTime(String alarmTime) {
        this.AlarmTime = alarmTime;
    }

    public int getIsChecked() {
        return IsChecked;
    }

    public void setIsChecked(int isChecked) {
        this.IsChecked = isChecked;
    }
}
