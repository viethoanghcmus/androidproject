package com.example.mymap;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.example.mymap.GroupData.Group;
import com.example.mymap.GroupData.GroupDAO;
import com.example.mymap.GroupData.MemberGroup;
import com.example.mymap.UserData.MyGroup;
import com.example.mymap.UserData.UserData;
import com.example.mymap.UserData.UserDataDAO;
import com.example.mymap.UserInfo.UserInfoDAO;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.mymap.MapsActivity.createCustomMarker;


public class GroupFragment extends Fragment {

    View rootView;
    View createGroupView;
    View showGroupView;
    AlertDialog.Builder builder;
    TextView exitGroup;
    FloatingActionButton create;
    TextView groupName;
    Button btnAddMenber;
    Group myGroup;
    //String GroupID;
    //List<MemberGroup> memberGroupList;
    //List<UserData>memberDataList;
    boolean isHaveGroup = false;

    CardView cardViews1, cardViews2, cardViews3, cardViews4, cardViews5;
    TextView nameMember1, nameMember2, nameMember3, nameMember4, nameMember5;
    CircleImageView avatarMember1, avatarMember2, avatarMember3, avatarMember4, avatarMember5;
    ImageView shareMember1,shareMember2,shareMember3,shareMember4,shareMember5;
    TextView countMember;

    Switch swShare, swShow;
    int pos;
    TextView statusShare;
    TextView statusShow;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_fragment_group, container, false);
        createGroupView = rootView.findViewById(R.id.createGroupView);
        showGroupView = rootView.findViewById(R.id.showGroupView);
        exitGroup = rootView.findViewById(R.id.exitGroup);
        create = rootView.findViewById(R.id.btnCreate);
        groupName = rootView.findViewById(R.id.groupName);
        btnAddMenber = rootView.findViewById(R.id.add_member);

        cardViews1 = rootView.findViewById(R.id.member1);
        cardViews2 = rootView.findViewById(R.id.member2);
        cardViews3 = rootView.findViewById(R.id.member3);
        cardViews4 = rootView.findViewById(R.id.member4);
        cardViews5 = rootView.findViewById(R.id.member5);

        nameMember1 = rootView.findViewById(R.id.name_member1);
        nameMember2 = rootView.findViewById(R.id.name_member2);
        nameMember3 = rootView.findViewById(R.id.name_member3);
        nameMember4 = rootView.findViewById(R.id.name_member4);
        nameMember5 = rootView.findViewById(R.id.name_member5);

        shareMember1 = rootView.findViewById(R.id.share_member1);
        shareMember2 = rootView.findViewById(R.id.share_member2);
        shareMember3 = rootView.findViewById(R.id.share_member3);
        shareMember4 = rootView.findViewById(R.id.share_member4);
        shareMember5 = rootView.findViewById(R.id.share_member5);

        myGroup = new Group();

        countMember = rootView.findViewById(R.id.subtitle_member_count);

        swShare = rootView.findViewById(R.id.swShare);
        swShow = rootView.findViewById(R.id.swShow);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder = new AlertDialog.Builder(getActivity());
                final EditText et = new EditText(getActivity());
                builder.setView(et);
                builder.setCancelable(true)
                        .setTitle("Nhập tên nhóm")
                        .setPositiveButton("Huỷ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Xong", null);
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();
                Button finnish = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                finnish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (et.getText().toString().equals("")) {
                            Toast.makeText(getActivity(), "Không được để trống", Toast.LENGTH_SHORT).show();
                        } else {
                            groupName.setText(et.getText().toString());
                            viewHaveGroup();
                            myGroup.GID = GroupDAO.CreateGroup(et.getText().toString());
                            GroupDAO.ChangeShareLocation(UserInfoDAO.fireUser.getUid(), true);
                            swShare.setChecked(true);
                            Log.d("log1", "group ID: " + myGroup.GID);
                            alertDialog.dismiss();
                        }
                    }
                });
            }
        });

        exitGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder = new AlertDialog.Builder(getActivity());
                builder.setCancelable(true)
                        .setTitle("Bạn muốn rời nhóm?")
                        .setPositiveButton("Không", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Đồng ý", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                GroupDAO.OutGroup(UserInfoDAO.fireUser.getUid());
                                viewNotHaveGroup();
                                //Hàm xoá thành viên hỏi group
                            }
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
        btnAddMenber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder = new AlertDialog.Builder(getActivity());
                final EditText etEmailMember = new EditText(getActivity());
                etEmailMember.setHint("Nhập bất kì auto thêm");
                builder.setCancelable(true)
                        .setView(etEmailMember)
                        .setTitle("Nhập email của thành viên muốn thêm")
                        .setPositiveButton("Huỷ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Xong", null);
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();

                Button finnish = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                //Lấy mGroupID
                finnish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (etEmailMember.getText().toString().equals("")) {
                            Toast.makeText(getActivity(), "Không được để trống", Toast.LENGTH_SHORT).show();
                        } else {
                            groupName.setText(etEmailMember.getText().toString());
                            ValueEventListener myGroupListener = new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {

                                    // Code xử lý bỏ đây

                                    myGroup.GID = (String) dataSnapshot.child(UserInfoDAO.mGroupRef).getValue();
                                    //Toast.makeText(getActivity(),"Email: "+ etEmailMember.getText(),Toast.LENGTH_SHORT).show();
                                    //Toast.makeText(getActivity(),"GID found: "+ GroupID,Toast.LENGTH_SHORT).show();
                                    //Lấy UID theo email
                                    ValueEventListener emailListener = new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {

                                            for (DataSnapshot UserSnapshot : dataSnapshot.getChildren()) {
                                                Log.d("AddUser: ", "UID in list: " + UserSnapshot.getKey() + "  " + "Email in list: " +
                                                        UserSnapshot.child(UserInfoDAO.EmailRef).getValue().toString() + "Email input: " + etEmailMember.getText());
                                                Log.d("AddUser:", "bool compare: " + UserSnapshot.child(UserInfoDAO.EmailRef).
                                                        getValue().toString().equals(etEmailMember.getText().toString()));
                                                if (UserSnapshot.child(UserInfoDAO.EmailRef).getValue().toString().equals(etEmailMember.getText().toString())) {
                                                    String UID = UserSnapshot.getKey();
                                                    Log.d("AddUser: ", "UID found: " + UID);
                                                    String mGroup = UserSnapshot.child(UserInfoDAO.mGroupRef).getValue().toString();
                                                    Log.d("AddUser: ", "mGroup found: " + mGroup);
                                                    if (mGroup.equals("0")) {
                                                        //Chưa trong group
                                                        GroupDAO.AddMemBer(myGroup.GID, UID, true);
                                                        UserInfoDAO.UpdateGroup(UID, myGroup.GID);
                                                        Log.d("Add success", UserSnapshot.getKey());
                                                        Toast.makeText(getActivity(), "Thêm thành công!", Toast.LENGTH_SHORT).show();
                                                        onResume();//Gọi để load lại
                                                        alertDialog.dismiss();
                                                    }
                                                    {
                                                        //Đã trong group
                                                        Toast.makeText(getActivity(), "Người dùng không tồn tại hoặc đã trong một group nào đó", Toast.LENGTH_SHORT).show();
                                                    }

                                                    // Xử lý thêm ở đây


                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            // Getting Post failed, log a message
                                            Log.w("group", "loadPost:onCancelled", databaseError.toException());
                                            // ...
                                        }
                                    };

                                    UserInfoDAO.UserInfoServer.addListenerForSingleValueEvent(emailListener);

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    // Getting Post failed, log a message
                                    Log.w("group", "loadPost:onCancelled", databaseError.toException());
                                    // ...
                                }
                            };
                            UserInfoDAO.UserInfoServer.child(UserInfoDAO.fireUser.getUid()).addListenerForSingleValueEvent(myGroupListener);


                        }
                    }
                });
            }
        });


        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        myGroup = new Group();
        final ValueEventListener myGroupListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                myGroup.GID = (String) dataSnapshot.child(UserInfoDAO.mGroupRef).getValue();
                if (myGroup.GID.equals("0")) {
                    viewNotHaveGroup();
                }
                if (!myGroup.GID.equals("0")) {
                    viewHaveGroup();
                    //Load data gồm tên hình và hiển thị CardView

                    //lấy list members
                    ValueEventListener memberListener = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            final CardView[] cardViews = {cardViews1, cardViews2, cardViews3, cardViews4, cardViews5};
                            final TextView[] nameMembers = {nameMember1, nameMember2, nameMember3, nameMember4, nameMember5};
                            final CircleImageView[] avatarMembers = {avatarMember1, avatarMember2, avatarMember3, avatarMember4, avatarMember5};
                            final ImageView [] shareMember = {shareMember1,shareMember2,shareMember3,shareMember4,shareMember5};
                            // Code xử lý ở đây

                            //memberDataList = new ArrayList<>();
                            //Lấy tên nhóm
                            groupName.setText(dataSnapshot.child(GroupDAO.GroupNameValue).getValue().toString());
                            //Lấy UID va ShareStatus của listMemberGroup
                            for (DataSnapshot memberSnapshot : dataSnapshot.child(GroupDAO.StrMembersRef).getChildren()) {
                                MemberGroup member = new MemberGroup();
                                member.setUID(memberSnapshot.getKey());

                                if (memberSnapshot.child(GroupDAO.StrMembersShare).getValue() != null) {
                                    member.setShareLocation((boolean) memberSnapshot.child(GroupDAO.StrMembersShare).getValue());
                                }
                                myGroup.listMembers.add(member);
                                Log.d("size", "onDataChange: "+myGroup.listMembers.size() );
                            }
                            for (int i =0;i<myGroup.listMembers.size();i++){
                                myGroup.listMembers.get(i).setCardView(cardViews[i]);
                                myGroup.listMembers.get(i).setCardName(nameMembers[i]);
                                myGroup.listMembers.get(i).setCardImage(avatarMembers[i]);
                                myGroup.listMembers.get(i).setCardShare(shareMember[i]);

                                if (myGroup.listMembers.get(i).isShareLocation()){
                                    shareMember[i].setBackgroundResource(R.drawable.circle_green);
                                }else{
                                    shareMember[i].setBackgroundResource(R.drawable.circlr_red);
                                }
                            }


                            //Get Name, Email
                            for (final MemberGroup member : myGroup.listMembers) {

                                ValueEventListener userInfoListener = new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        member.setName(dataSnapshot.child(UserInfoDAO.DisplayNameRef).getValue().toString());
                                        member.setEmail(dataSnapshot.child(UserInfoDAO.EmailRef).getValue().toString());

                                        Log.d("log111", "onSuccess: ok" + myGroup.listMembers.size());
                                        member.getCardView().setVisibility(View.VISIBLE);
                                        member.getCardName().setText(member.getName());

                                        //member.getCardImage().setImageURI(member.getAvatar());


                                        Log.d("log111", "onDataChange: "+member.getName());

                                        if (member.getUID().equals(FirebaseAuth.getInstance().getUid())){
                                            swShare.setChecked(member.isShareLocation());
                                        }

                                        // hình load k đc
                                        FirebaseStorage storage = FirebaseStorage.getInstance();
                                        String fileName = "avatar.jpg";
                                        String Path = member.getUID() + "/" + fileName;

                                        StorageReference storageRef = storage.getReference(Path);

                                        final File localFile;
                                        try {

                                            File tempFile = new File(member.getUID());
                                            boolean exists = tempFile.exists();
                                            if (exists == true) {
                                                tempFile.delete();
                                            }
                                            localFile = File.createTempFile(member.getUID(), "jpg");
                                            storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                                @Override
                                                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                                    // Local temp file has been created
                                                    Uri avatar_Uri = Uri.fromFile(localFile);
                                                    member.setAvatar(avatar_Uri);

                                                    //Đã lấy đc full dữ liệu
                                                    //Xử lí hiện thị từng member


                                                }
                                            }).addOnFailureListener(new OnFailureListener() {

                                                @Override
                                                public void onFailure(@NonNull Exception exception) {
                                                    // Handle any errors
                                                }
                                            });
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        // Getting Post failed, log a message
                                        Log.w("e", "loadPost:onCancelled", databaseError.toException());
                                    }
                                };
                                UserInfoDAO.UserInfoServer.child(member.getUID()).addListenerForSingleValueEvent(userInfoListener);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            // Getting Post failed, log a message
                            Log.w("group", "loadPost:onCancelled", databaseError.toException());
                            // ...
                        }
                    };
                    GroupDAO.GroupDataServer.child(myGroup.GID).addListenerForSingleValueEvent(memberListener);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("group", "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };
        UserInfoDAO.UserInfoServer.child(UserInfoDAO.fireUser.getUid()).addListenerForSingleValueEvent(myGroupListener);

        //Sw share location
        swShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GroupDAO.ChangeShareLocation(FirebaseAuth.getInstance().getUid(),swShare.isChecked());
                onResume();
            }
        });
        statusShare = rootView.findViewById(R.id.subtitle_show_detail);
        swShow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    statusShare.setText("Đang bật");
                    statusShare.setTextColor(Color.GREEN);
                }else {
                    statusShare.setText("Đang tắt");
                    statusShare.setTextColor(Color.RED);
                }
            }
        });

        //Sw show marker
        swShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        statusShow = rootView.findViewById(R.id.subtitle_share_detail);
        swShare.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    statusShow.setText("Chia sẻ");
                    statusShow.setTextColor(Color.GREEN);
                }else {
                    statusShow.setText("Bí mật");
                    statusShow.setTextColor(Color.RED);
                }
            }
        });
    }

    public void viewHaveGroup() {
        createGroupView.setVisibility(View.GONE);
        showGroupView.setVisibility(View.VISIBLE);
    }

    public void viewNotHaveGroup() {
        createGroupView.setVisibility(View.VISIBLE);
        showGroupView.setVisibility(View.GONE);
    }
}
