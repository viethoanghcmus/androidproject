package com.example.mymap.UserData;

public class UserData {

    private double _lat;
    private double _long;

    public UserData(double _lat, double _long) {
        this._lat = _lat;
        this._long = _long;
    }

    public double get_lat() {
        return _lat;
    }

    public void set_lat(double _lat) {
        this._lat = _lat;
    }

    public double get_long() {
        return _long;
    }

    public void set_long(double _long) {
        this._long = _long;
    }
}

