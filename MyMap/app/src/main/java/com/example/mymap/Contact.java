package com.example.mymap;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Contact extends AppCompatActivity {
    public EditText edt_Sub, edt_Mes;
    public Button btn_Send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_contact);
        edt_Sub = (EditText) findViewById(R.id.edt_Subject);
        edt_Mes = (EditText) findViewById(R.id.edt_Message);
        btn_Send = (Button) findViewById(R.id.btn_send);

        btn_Send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMail();
            }
        });
    }

    public void sendMail() {
        String recipientList = "nguyenhoangkk99@gmail.com"; // thêm đến 1 email khác thì để thêm dấu phẩy và mail ở sau
        String[] recipients = recipientList.split(",");
        String subject = edt_Sub.getText().toString().trim();
        String message = edt_Mes.getText().toString().trim();
        Intent intent = new Intent(Intent.ACTION_SENDTO);

        intent.putExtra(Intent.EXTRA_EMAIL, recipients);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, message);
        intent.setType("text/plain");
        intent.setData(Uri.parse("mailto:"));
        startActivity(intent);
    }
}

