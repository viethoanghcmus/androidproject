package com.example.mymap;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.util.ArrayList;



public class Sound extends Service {
    public static ArrayList<MediaPlayer> mediaPlayers = new ArrayList<>();
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("Toi trong Sound","Xin chao");
        String key=intent.getStringExtra("extra");
        int index=intent.getIntExtra("index",-1);
        int isDeleteMedia=intent.getIntExtra("deletemedia",0);
        if (key.equals("on")){
            if (mediaPlayers.size() <= index)
                mediaPlayers.add(MediaPlayer.create(this,R.raw.nhac));
            else
                mediaPlayers.set(index, MediaPlayer.create(this,R.raw.nhac));
            mediaPlayers.get(index).start();
//            FormSavePlaceActivity.switch1.get(index).setChecked(false);
        }else{
            if (mediaPlayers.size() > index && index!=-1){
                mediaPlayers.get(index).stop();
                FormSavePlaceActivity.switch1.get(index).setChecked(false);
                if (mediaPlayers.size() <= index)
                    mediaPlayers.add(MediaPlayer.create(this,R.raw.nhac));
                else
                    mediaPlayers.set(index, MediaPlayer.create(this,R.raw.nhac));
            }
        }
        return START_NOT_STICKY;
    }
}
