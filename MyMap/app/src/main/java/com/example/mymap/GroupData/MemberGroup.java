package com.example.mymap.GroupData;

import android.net.Uri;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.google.android.gms.maps.model.Marker;

import de.hdodenhof.circleimageview.CircleImageView;

public class MemberGroup {
    String UID;
    String Name;
    String Email;
    Uri Avatar;
    boolean ShareLocation;
    Marker marker = null;
    CardView cardView = null;
    TextView cardName = null;
    CircleImageView cardImage = null;
    ImageView cardShare = null;

    public MemberGroup(String UID, String name, String email, Uri avatar, boolean shareLocation) {
        this.UID = UID;
        Name = name;
        Email = email;
        Avatar = avatar;
        ShareLocation = shareLocation;
    }

    public MemberGroup() { }

    public String getUID() {
        return UID;
    }

    public String getName() {
        return Name;
    }

    public String getEmail() {
        return Email;
    }

    public Uri getAvatar() {
        return Avatar;
    }

    public boolean isShareLocation() {
        return ShareLocation;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public void setAvatar(Uri avatar) {
        Avatar = avatar;
    }

    public void setShareLocation(boolean shareLocation) {
        ShareLocation = shareLocation;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public CardView getCardView() {
        return cardView;
    }

    public void setCardView(CardView cardView) {
        this.cardView = cardView;
    }

    public TextView getCardName() {
        return cardName;
    }

    public CircleImageView getCardImage() {
        return cardImage;
    }

    public void setCardName(TextView cardName) {
        this.cardName = cardName;
    }

    public void setCardImage(CircleImageView cardImage) {
        this.cardImage = cardImage;
    }

    public ImageView getCardShare() {
        return cardShare;
    }

    public void setCardShare(ImageView cardShare) {
        this.cardShare = cardShare;
    }
}

