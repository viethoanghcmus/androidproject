package com.example.mymap.GroupData;

import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.mymap.UserInfo.UserInfoDAO;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class GroupDAO {

    public static String StrGroupRef = "Group";
    public static String GroupNameValue = "GroupName";
    public static String StrMembersRef = "Members";
    public static String StrMembersID = "UID";
    public static String StrMembersShare = "ShareLocation";
    public static DatabaseReference GroupDataServer = FirebaseDatabase.getInstance().getReference().child(StrGroupRef);
    private static FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    //Trả về GroupID của UID
    //Nếu bằng 0 không trong bất kì nhóm nào

    public static void GetMyGroup(final String UID){
        ValueEventListener myGroupListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("group", "loadPost:onCancelled", databaseError.toException());

                // ...
            }
        };
        UserInfoDAO.UserInfoServer.child(UID).addListenerForSingleValueEvent(myGroupListener);
        Log.d("group","beginWait");
    }
    //trả về GroupID mới tạo
    public static String CreateGroup( String GroupName) {
        String newGroupID = GroupDataServer.push().getKey();
        GroupDataServer.child(newGroupID).child(GroupNameValue).setValue(GroupName);
        AddMemBer(newGroupID,user.getUid(),false);
        UserInfoDAO.UpdateGroup(user.getUid(),newGroupID);
        return newGroupID;
    }

    //Thêm thành viên cho nhóm
    public static void AddMemBer(String GID, String UID, boolean ShareLocation) {
        GroupDataServer.child(GID).child(StrMembersRef).child(UID).child(StrMembersID).setValue(UID);
        GroupDataServer.child(GID).child(StrMembersRef).child(UID).child(StrMembersShare).setValue(ShareLocation);
    }

    public static List<MemberGroup> GetListMember(final String GroupID) {


        getListMember(GroupID);
        //while (!getDone)
        {
        }

        return listResult;
    }

    public static List<MemberGroup> listResult = new ArrayList<MemberGroup>();

    private static boolean getDone;

    public static void getListMember(String GroupID) {
        getDone = false;
        ValueEventListener memberListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                for (DataSnapshot memberSnapshot : dataSnapshot.child(StrMembersRef).getChildren()) {
                    MemberGroup member = new MemberGroup();
                    member.UID = memberSnapshot.getKey();
                    member.ShareLocation = (boolean) memberSnapshot.child(StrMembersShare).getValue();
                    listResult.add(member);
                }
                getDone = true;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("group", "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };
        GroupDataServer.child(GroupID).child(StrMembersRef).addListenerForSingleValueEvent(memberListener);
    }






    public static void ChangeShareLocation(final String UID,final boolean Status) {
        ValueEventListener myGroupListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                String mGroup = dataSnapshot.child(UserInfoDAO.mGroupRef).getValue().toString();
                Log.d("mGroup", mGroup);
                GroupDataServer.child(mGroup).child(StrMembersRef).child(UID).child(StrMembersShare).setValue(Status);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("group", "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };
        UserInfoDAO.UserInfoServer.child(UID).addListenerForSingleValueEvent(myGroupListener);
    }

    public static void OutGroup(final String UID){
        ValueEventListener myGroupListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                String mGroup = dataSnapshot.child(UserInfoDAO.mGroupRef).getValue().toString();
                Log.d("mGroup", mGroup);
                UserInfoDAO.UpdateGroup(user.getUid(),"0");
                GroupDataServer.child(mGroup).child(StrMembersRef).child(UID).child(StrMembersID).setValue(null);
                GroupDataServer.child(mGroup).child(StrMembersRef).child(UID).child(StrMembersShare).setValue(null);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("group", "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };
        UserInfoDAO.UserInfoServer.child(UID).addListenerForSingleValueEvent(myGroupListener);

    }
}
