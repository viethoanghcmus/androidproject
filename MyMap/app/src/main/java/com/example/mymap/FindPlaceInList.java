package com.example.mymap;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

public class FindPlaceInList extends AppCompatActivity {
    public static int RETURN_CODE = 1999;
    GridView gvPlaces;
    ArrayList<Places> arrayPlaces;
    PlacesAdapter placesAdapter;
    Toolbar toolbar;
    ImageView imgBack;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_find_place_in_list);
        AnhXa();
        placesAdapter= new PlacesAdapter(this,R.layout.listview_places,arrayPlaces);
        gvPlaces.setAdapter(placesAdapter);
        gvPlaces.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent();
                switch (arrayPlaces.get(i).getIcon()){
                    case R.drawable.atm2:
                        putExtra(intent,1);
                        break;
                    case R.drawable.cafe:
                        putExtra(intent,2);
                        break;
                    case R.drawable.gym:
                        putExtra(intent,3);
                        break;
                    case R.drawable.library:
                        putExtra(intent,4);
                        break;
                    case R.drawable.hospital:
                        putExtra(intent,5);
                        break;
                    case R.drawable.pharmacy:
                        putExtra(intent,6);
                        break;
                    case R.drawable.restaurant:
                        putExtra(intent,7);
                        break;
                    case R.drawable.school:
                        putExtra(intent,8);
                        break;
                    case R.drawable.supermarket:
                        putExtra(intent,9);
                        break;
                    case R.drawable.bakery:
                        putExtra(intent,10);
                        break;
                    case R.drawable.busstation:
                        putExtra(intent,11);
                        break;
                    case R.drawable.church:
                        putExtra(intent,12);
                        break;
                    case R.drawable.dentist:
                        putExtra(intent,13);
                        break;
                    case R.drawable.spa:
                        putExtra(intent,14);
                        break;
                    case R.drawable.shoes:
                        putExtra(intent,15);
                        break;
                }
            }
        });
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        imgBack=(ImageView)findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent();
                intent.putExtra("num",0);
                setResult(RETURN_CODE,intent);
                finish();
            }
        });
    }

    private void putExtra(Intent intent,int number) {
        intent.putExtra("num",number);
        setResult(RETURN_CODE,intent);
        finish();
    }


    private void AnhXa() {
        gvPlaces=(GridView)findViewById(R.id.gridviewPlaces);
        arrayPlaces=new ArrayList<>();
        arrayPlaces.add(new Places(R.drawable.atm2,"ATM"));
        arrayPlaces.add(new Places(R.drawable.cafe,"Quán Cafe"));
        arrayPlaces.add(new Places(R.drawable.gym,"Phòng Gym"));
        arrayPlaces.add(new Places(R.drawable.library,"Thư viên"));
        arrayPlaces.add(new Places(R.drawable.hospital,"Bệnh viện"));
        arrayPlaces.add(new Places(R.drawable.pharmacy,"Nhà thuốc"));
        arrayPlaces.add(new Places(R.drawable.restaurant,"Nhà hàng"));
        arrayPlaces.add(new Places(R.drawable.school,"Trường học"));
        arrayPlaces.add(new Places(R.drawable.supermarket,"Siêu thị"));
        arrayPlaces.add(new Places(R.drawable.bakery,"Tiệm bánh mì"));
        arrayPlaces.add(new Places(R.drawable.busstation,"Trạm xe bus"));
        arrayPlaces.add(new Places(R.drawable.church,"Nhà thờ"));
        arrayPlaces.add(new Places(R.drawable.dentist,"Nha khoa"));
        arrayPlaces.add(new Places(R.drawable.spa,"Spa"));
        arrayPlaces.add(new Places(R.drawable.shoes,"Cửa hàng giày dép"));


    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent();
        intent.putExtra("num",0);
        setResult(RETURN_CODE,intent);
        finish();
        super.onBackPressed();
    }
}
