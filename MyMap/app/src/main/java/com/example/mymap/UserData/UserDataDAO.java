package com.example.mymap.UserData;

import android.util.Log;

import com.example.mymap.Database;
import com.example.mymap.SavedPlace;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class UserDataDAO {

    public  static String UserDataRef = "UserData";
    public static void UploadUserData(UserData data) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            FirebaseDatabase db = FirebaseDatabase.getInstance();
            DatabaseReference myRef = db.getReference("UserData");
            String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            myRef.child(uid).setValue(data);
        }
    }

    //Tùy chình trong phần ondataChange để  cập nhập dữ liệu lên UI
    public static void GetUserData(final String UID) {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        ValueEventListener userDataListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                // Get Post object and use the values to update the UI
                UserData user = dataSnapshot.child(UID).getValue(UserData.class);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message

                // ...
            }
        };
        DatabaseReference myRef = database.getReference().child(UserDataRef);
        myRef.addListenerForSingleValueEvent(userDataListener);
    }
}

