package com.example.mymap.Login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mymap.Database;
import com.example.mymap.GroupData.GroupDAO;
import com.example.mymap.R;
import com.example.mymap.Ult;
import com.example.mymap.UserData.SavedPlaceDAO;
import com.example.mymap.UserInfo.UserInfoDAO;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Register extends AppCompatActivity implements View.OnClickListener {

    public FirebaseUser newUser;
    private FirebaseAuth mAuth;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();


        Button RegisterButton = findViewById(R.id.RegisterButton);
        Button backToLogin = findViewById(R.id.backToLoginButton);
        Button resetPasswordButton = findViewById(R.id.btn_reset_password);
        EditText nameEditText = findViewById(R.id.NameEditText);
        progressBar = findViewById(R.id.progressBar);
        RegisterButton.setOnClickListener(this);
        backToLogin.setOnClickListener(this);
        resetPasswordButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.RegisterButton: {
                UserRegister();
                break;
            }
            case R.id.backToLoginButton: {
                finish();
                break;
            }
            case R.id.btn_reset_password: {
                finish();
                Intent intent = new Intent(Register.this, ResetPassword.class);
                startActivity(intent);
                break;
            }
        }
    }



    private void ShowMessengerResult(String m) {
        TextView mess = findViewById(R.id.messengerTextView);
        mess.setText(m);
    }

    private boolean UserRegister() {
        progressBar.setVisibility(View.VISIBLE);

        EditText EmailEditText = findViewById(R.id.EmailEditText);
        EditText passEditText = findViewById(R.id.passEditText);
        EditText confirmPassEditText = findViewById(R.id.confirmPassEditText);
        EditText nameEditText = findViewById(R.id.NameEditText);

        String email = EmailEditText.getText().toString();
        String password = passEditText.getText().toString();
        String SecondPassword = confirmPassEditText.getText().toString();
        final String name = nameEditText.getText().toString();

        if (email.length() == 0 || password.length() == 0) {
            Toast.makeText(getApplicationContext(), "Email và mật khẩu không được để trống !",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!SecondPassword.equals(password)) {
            ShowMessengerResult("Mật khẩu không khớp nhau !");
            return false;
        }
        if (name.length()<3) {
            ShowMessengerResult("Tên hiện thị phải dài hơn !");
            return false;
        }

        //Task<AuthResult> taskAuth =  mAuth.createUserWithEmailAndPassword(email,password);
        Task<AuthResult> authResultTask = mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {


                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()) {

                            newUser = task.getResult().getUser();
                            FirebaseUser user =  task.getResult().getUser();


                            UserInfoDAO.UpdateEmail(user.getUid(),user.getEmail());
                            UserInfoDAO.UpdateGroup(user.getUid(),"0");
                            UserInfoDAO.UpdateNameDisplay(user.getUid(),name);

                            Ult.showAlertDialog(Register.this, "Đăng ký thành công", "Bạn đã đăng ký tài khoản thành công");

                            Login.DeleteAllAlarm();
                            Database db = new Database(getApplication(),"database.sqlite",null,1);
                            SavedPlaceDAO savedPlaceDAO = new SavedPlaceDAO(db);
                            savedPlaceDAO.GetSavedPlaceDataAndSetAlarm(FirebaseAuth.getInstance().getCurrentUser().getUid(),getApplicationContext());
                            GroupDAO.GetMyGroup(FirebaseAuth.getInstance().getCurrentUser().getUid());
                            finish();
                        } else {
                            ShowMessengerResult(task.getException().getMessage());
                        }
                    }
                });

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }

}
