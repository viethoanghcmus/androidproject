package com.example.mymap;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.mymap.GroupData.MemberGroup;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;

public class Database  extends SQLiteOpenHelper {

    public Database(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void QueryData(String sql)
    {
        SQLiteDatabase database=getWritableDatabase();
        database.execSQL(sql);
    }

    public Cursor GetData(String sql){
        SQLiteDatabase database=getReadableDatabase();
        return database.rawQuery(sql,null);
    }

    public void InsertSavedPlace(SavedPlace savedPlace)
    {
        String name=savedPlace.getName();
        String note=savedPlace.getNote();
        String address=savedPlace.getAddress();
        double latitude=parseDouble(savedPlace.getLatitude());
        double longitude=parseDouble(savedPlace.getLongitude());
        String AlarmTime=savedPlace.getAlarmTime();
        int IsChecked=savedPlace.getIsChecked();
        SQLiteDatabase database=getWritableDatabase();
        database.execSQL("INSERT INTO SavedPlace values" +
                "(" + null + ",'" + name + "','" + note
                + "','" + address + "','" + latitude + "','" + longitude + "','" + AlarmTime + "',"+IsChecked+")");
    }

    public  void CreateTableSavedPlace()
    {
        SQLiteDatabase database=getWritableDatabase();
        database.execSQL("CREATE TABLE IF NOT EXISTS SavedPlace" +
                "(Id INTEGER PRIMARY KEY AUTOINCREMENT,Name VARCHAR(30),Note VARCHAR(300),Address VARCHAR(300)," +
                "Latitude NVARCHAR(20),Longitude NVARCHAR(20),AlarmTime NVARCHAR(50),IsChecked INT)");
    }

    public  void ClearTableSavedPlace()
    {
        SQLiteDatabase db=getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS SavedPlace");
        CreateTableSavedPlace();
    }
    public int CountSavedPlace()
    {
        SQLiteDatabase database=getReadableDatabase();
        Cursor tmp=database.rawQuery("select * from SavedPlace",null);
        if (tmp==null) return 0;
        return tmp.getCount();
    }
    public  void CreateMemberGroup()
    {
        SQLiteDatabase database=getWritableDatabase();
        database.execSQL("CREATE TABLE IF NOT EXISTS MemberGroup" +
                "(UID INTEGER PRIMARY KEY AUTOINCREMENT, Latitude NVARCHAR(20),Longitude NVARCHAR(20),IsShared INT)");
    }
    public void InsertMemberGroup(MemberGroup memberGroup)
    {
        String name=memberGroup.getName();
//        double latitude=parseDouble(memberGroup.get());
//        double longitude=parseDouble(memberGroup.getLongitude());
//        String AlarmTime=savedPlace.getAlarmTime();
//        int IsChecked=savedPlace.getIsChecked();
//        SQLiteDatabase database=getWritableDatabase();
//        database.execSQL("INSERT INTO SavedPlace values" +
//                "(" + null + ",'" + name + "','" + note
//                + "','" + address + "','" + latitude + "','" + longitude + "','" + AlarmTime + "',"+IsChecked+")");
    }
    public  void CreateTableIsShow()
    {
        SQLiteDatabase database=getWritableDatabase();
        database.execSQL("CREATE TABLE IF NOT EXISTS IsShow (IsShow INT)");
    }

    public void UpdateIDSavedPlace()
    {
        SQLiteDatabase db=getWritableDatabase();
        Cursor tmp=db.rawQuery("select * from SavedPlace",null);
        for (int i=1;i<=CountSavedPlace();i++)
        {
            tmp.moveToNext();
            String idTmp=tmp.getString(0);
            db.execSQL("Update SavedPlace set Id="+i+" where Id="+idTmp);

        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


}
